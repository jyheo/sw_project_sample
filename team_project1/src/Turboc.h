// 혼자 연구하는 C/C++의 도우미 헤더 파일
// 비주얼 C++ 환경에서 터보 C 스타일의 함수를 정의한다.
#ifndef TURBOC_HEADER
#define TURBOC_HEADER

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <windows.h>

typedef enum { NOCURSOR, SOLIDCURSOR, NORMALCURSOR } CURSOR_TYPE;
void clrscr();
void gotoxy(int x, int y);
int wherex();
int wherey();
void setcursortype(CURSOR_TYPE c);

#define delay(n) Sleep(n)							// n/1000초만큼 시간 지연
#define randomize() srand((unsigned)time(NULL))		// 난수 발생기 초기화
#define random(n) (rand() % (n))					//0~n까지의 난수 발생

// 이 매크로가 정의되어 있으면 함수의 원형만 선언하고 정의는 하지 않는다.
#ifndef TURBOC_PROTOTYPE_ONLY


// 커서를 숨기거나 다시 표시한다.
void setcursortype(CURSOR_TYPE c);
// 커서의 x 좌표를 조사한다.
int wherex();
// 커서의 y좌표를 조사한다.
int wherey();
void gotoxy(int x, int y);
void clrscr();

#endif // TURBOC_PROTOTYPE_ONLY
#endif // TURBOC_HEADER
