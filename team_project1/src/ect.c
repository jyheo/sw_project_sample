#include "common.h"
#include "Turboc.h"

void showTotal()
{
	clearResult();
	gotoxy(0,2);
	printf("┌──────<디렉토리>──────┐	 ┌──────<파일>───────┐\n");
	printf("│                                  │   │                                │\n");
	printf("│디렉토리 관리 :  /di 디렉토리명   │   │파일 이동  :  /fm 파일명        │\n");
	printf("│디렉토리 이동 :  /dm 디렉토리명   │   │파일 생성  :  /fn 파일명        │\n");
	printf("│디렉토리 생성 :  /dd 디렉토리명   │   │파일 삭제  :  /fd 파일명        │\n");
	printf("│디렉토리 복사 :  /dc 디렉토리명   │   │파일 복사  :  /fc 파일명        │\n");
	printf("│디렉토리 정렬 :  /ds a (이름순)   │   │파일 속성  :  /fv 파일명        │\n");
	printf("│디렉토리 정렬 :  /ds d (날짜순)   │   │파일 실행  :  /fp 파일명        │\n");
	printf("│디렉토리 정렬 :  /ds s (크기순)   │   └────────────────┘\n");
	printf("│*오름차순일 경우 명령어 뒤에 u,   │   ┌──────<기타>───────┐ \n│내림차순일 경우 뒤에 d를 붙임.    │   │시간 보기   : /time             │\n");
	printf("│/ds au 일 경우 이름 오름차순,     │   │도움말 보기 : /help             │\n│/ds ad 일 경우 이름 내림차순.     │   │국가설정(시간): /settime        │\n");
	printf("└─────────────────┘   └────────────────┘\n");
	printResult("계속하시려면 아무키나 눌러주시기 바랍니다.");
	fflush(stdin);
	getchar();

}

void showPartition()
{
	int select=0;
	clearResult();
	gotoxy(0,2);
	printf("1. 디렉토리\n");
	printf("2. 파일\n");
	printf("3. 기타기능\n");
	printResult("");
	while(!(select>=1 && select <= 3))
	{
		scanf("%d", &select);
	}
	switch(select)
	{
	case 1:
		clearResult();
		gotoxy(0,2);
		printf("┌──────<디렉토리>──────┐\n");
		printf("│                                  │\n");
		printf("│디렉토리 관리 :  /di 디렉토리명   │\n");
		printf("│디렉토리 이동 :  /dm 디렉토리명   │\n");
		printf("│디렉토리 생성 :  /dd 디렉토리명   │\n");
		printf("│디렉토리 복사 :  /dc 디렉토리명   │\n");
		printf("│디렉토리 정렬 :  /ds a (이름순)   │\n");
		printf("│디렉토리 정렬 :  /ds d (날짜순)   │\n");
		printf("│디렉토리 정렬 :  /ds s (크기순)   │\n");
		printf("│*오름차순일 경우 명령어 뒤에 u,   │\n");
		printf("│/ds au 일 경우 이름 오름차순,     │\n");
		printf("└─────────────────┘\n");
		break;
	case 2:
		clearResult();
		gotoxy(0,2);
		printf("┌──────<파일>───────┐\n");
		printf("│                                │\n");
		printf("│파일 이동  :  /fm 파일명        │\n");
		printf("│파일 생성  :  /fn 파일명        │\n");
		printf("│파일 삭제  :  /fd 파일명        │\n");
		printf("│파일 복사  :  /fc 파일명        │\n");
		printf("│파일 속성  :  /fv 파일명        │\n");
		printf("│파일 실행  :  /fp 파일명        │\n");
		printf("└────────────────┘\n");
		break;
	case 3:
		clearResult();
		gotoxy(0,2);
		printf("┌──────<기타>───────┐\n");
		printf("│도움말 보기   :  /help          │\n");
		printf("│시간 보기     :  /time          │\n");
		printf("│국가설정(시간):  /settime       │\n");
		printf("└────────────────┘\n");
		break;
	}
	printResult("계속하시려면 아무키나 눌러주시기 바랍니다.");
	fflush(stdin);
	getchar();
}
void searchHelp()
{
	char search[255] = "";
	clearResult();
	gotoxy(0,2);
	printf("                    ┌──────────────────┐\n");
	printf("                    │                                    │\n");
	printf("                    │    도           움          말     │\n");
	printf("                    │                                    │\n");
	printf("                    └──────────────────┘\n\n\n\n\n\n");
	printf("검색할 도움말을 입력하여 주시기 바랍니다.\n\n");
	printResult("도움말 검색에 접속하였습니다.(나가시려면 exit를 입력해주시기 바랍니다.)");
	while(strcmp(search,"exit") != 0)
	{
		gets(search);
		if(strcmp(search,"\/help")==0)
		{
			printResult("\/help : 도움말 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/di")==0)
		{
			printResult("\/di (디랙토리명 or 경로) : 디렉토리 이동 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/dn")==0)
		{
			printResult("\/dn (디렉토리명) : 디렉토리 생성 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/dc")==0)
		{
			printResult("\/dc (복사할 디렉토리명) (복사될 디렉토리명) : 디렉토리 복사 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/ds")==0)
		{
			printResult("\/ds (a-가나다, d-날짜, s-크기)+(u-오름차순, d-내림차순) : 디렉토리 정렬 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/dp")==0)
		{
			printResult("\/dp (w-가로보기, l-세로보기) : 디렉토리 보여주기방식설정 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/dcn")==0)
		{
			printResult("\/dcn (변경할 디렉토리명) (바꿀 디렉토리명) : 디렉토리 이름변경 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/dm")==0)
		{
			printResult("\/dm (잘라내기 할 디렉토리명) (잘라내기 될 디렉토리명) : 디렉토리 이름변경 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/fm")==0)
		{
			printResult("\/fm (파일명) (디렉토리명) : 파일 이동 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/fn")==0)
		{
			printResult("\/fn (파일명) : 파일 생성 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/fc")==0)
		{
			printResult("\/fc (복사할 파일명) (복사될 파일명) : 파일 복사 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/fv")==0)
		{
			printResult("\/fv (파일명) : 파일 상세보기- (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/fp")==0)
		{
			printResult("\/fp (파일명) : 파일실행 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
		else if(strcmp(search,"\/fcn")==0)
		{
			printResult("\/fcn (변경할 파일명) (바꿀 파일명) : 파일 이름변경 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}

		else if(strcmp(search,"\/time")==0)
		{
			printResult("\/time : 현재시간 출력- (나가시려면 exit를 입력해주시기 바랍니다.)");
		}

		else if(strcmp(search,"\/timeset")==0)
		{
			printResult("\/timeset : 국가 설정 - (나가시려면 exit를 입력해주시기 바랍니다.)");
		}
	}
	fflush(stdin);
}
void help()
{	
	int select=0;
	while(1)
	{
		clearResult();
		gotoxy(0,2);
		printf("                    ┌──────────────────┐\n");
		printf("                    │                                    │\n");
		printf("                    │    도           움          말     │\n");
		printf("                    │                                    │\n");
		printf("                    └──────────────────┘\n\n");
		printf("1. 전체보기\n\n");
		printf("2. 항목보기\n\n");
		printf("3. 검색\n\n");
		printf("4. 도움말 종료\n\n");
		printResult("도움말로 접속하였습니다.");
		scanf("%d", &select);
		fflush(stdin);
		switch(select)
		{
		case 1:
			showTotal();
			break;
		case 2:
			showPartition();
			break;
		case 3:
			searchHelp();
			break;
		case 4:
			return ;
		}
	}
}

void setTime(char *country)
{
	int select=0;
	clearResult();
	gotoxy(0,2);
	printf("국가를 설정하여 주십시오.(1~14번 中선택)\n");
	printf("1.한국\n");
	printf("2.일본\n");
	printf("3.중국\n");
	printf("4.미국\n");
	printf("5.호주\n");
	printf("6.영국\n");
	printf("7.두바이\n");
	printf("8.남아공\n");
	printf("9.캐나다\n");
	printf("10.멕시코\n");
	printf("11.브라질\n");
	printf("12.프랑스\n");
	printf("13.러시아\n");
	printf("14.인도네시아\n");

	printResult("");
	while(!(select >= 1 && select <=14))
	{
		scanf("%d",&select);
		fflush(stdin);
	}
	
	switch(select)
	{
	case 1:
		*country = 'k';
		break;
	case 2:
		*country = 'j';
		break;
	case 3:
		*country = 'c';
		break;
	case 4:
		*country = 'u';
		break;
	case 5:
		*country = 'h';
		break;
	case 6:
		*country = 'e';
		break;
	case 7:
		*country = 'd';
		break;
	case 8:
		*country = 'n';
		break;
	case 9:
		*country = 'a';
		break;
	case 10:
		*country = 'm';
		break;
	case 11:
		*country = 'b';
		break;
	case 12:
		*country = 'f';
		break;
	case 13:
		*country = 'r';
		break;
	case 14:
		*country = 'i';
		break;
	}
}
void getWorldTime(int tzOffset, char* buffer, size_t bufferSize) {
	time_t timer;
	struct tm t;
	timer = time(NULL); // 현재 시각을 초 단위로 얻기
	timer -= CURRENT_TIMEZONE_OFFSET * 3600;
	timer += tzOffset * 3600;
	localtime_s(&t, &timer); // 초 단위의 시간을 분리하여 구조체에 넣기
	sprintf_s(buffer, bufferSize, "%04d-%02d-%02d %02d:%02d:%02d",
		t.tm_year + 1900,
		t.tm_mon + 1,
		t.tm_mday,
		t.tm_hour,
		t.tm_min,
		t.tm_sec);
}
void showTime(char country)
{
	char buf[80];
	clearResult();
	gotoxy(0,2);
	switch(country)
	{
	case 'k':
		getWorldTime(+9, buf, sizeof(buf));
		printf("(한국)       : %s   ", buf);
		break;
	case 'j':
		getWorldTime(+9, buf, sizeof(buf));
		printf("(일본)       : %s\n\n", buf);
		break;
	case 'c':
		getWorldTime(+8, buf, sizeof(buf));
		printf("(중국)       : %s   ", buf);
		break;
	case 'u':
		getWorldTime(-5, buf, sizeof(buf));
	printf("(미국)       : %s   ", buf);
		break;
	case 'h':
		getWorldTime(+11, buf, sizeof(buf));
	printf("(호주)       : %s\n\n", buf);
		break;
	case 'e':
		getWorldTime(0, buf, sizeof(buf));
	printf("(영국)       : %s   ", buf);
		break;
	case 'd':
		getWorldTime(+4, buf, sizeof(buf));
	printf("(두바이)     : %s   ", buf);
		break;
	case 'n':
		getWorldTime(+2, buf, sizeof(buf));
	printf("(남아공)     : %s   ", buf);
		break;
	case 'a':
		getWorldTime(-8, buf, sizeof(buf));
	printf("(캐나다)     : %s\n", buf);
		break;
	case 'm':
		getWorldTime(-6, buf, sizeof(buf));
	printf("(멕시코)     : %s\n\n", buf);
		break;
	case 'b':
		getWorldTime(-2, buf, sizeof(buf));
	printf("(브라질)     : %s\n\n", buf);
		break;
	case 'f':
		getWorldTime(+1, buf, sizeof(buf));
	printf("(프랑스)     : %s\n\n", buf);
		break;
	case 'r':
		getWorldTime(+4, buf, sizeof(buf));
	printf("(러시아)     : %s\n\n", buf);
		break;
	case 'i':
		getWorldTime(+7, buf, sizeof(buf));
	printf("(인도네시아) : %s\n\n", buf);
		break;
	}
	printResult("계속하시려면 아무키나 눌러주시기 바랍니다.");
	getchar();
}