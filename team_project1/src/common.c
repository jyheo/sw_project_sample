#include "common.h"
#include "Turboc.h"

extern char currentPath[255];
extern char trashPath[255];

void start()
{
	struct _finddatai64_t c_file;
	intptr_t hFile;

	system("title ★ WCS ★");
	system("mode con: cols=100lines=26");
	printf("1.0Ver.\n\n");
	printf("안녕하세요. WCS을 사용해 주셔서 감사합니다.\n");
	printf("\n\nCopyright <#include> 2012.\n\n");
	printf("명령어 입력창에 명령어를 입력해 주세요.\n");
	printf("도움말을 보시려면 /help를 입력해주세요.\n");
	gotoxy(0,17);
	printf("────────────────────────────────────────────────────\n");
	getCurrentPath();
	printCurrentPath();
	strcpy(trashPath,currentPath);
	strcat(trashPath,"\\");
	strcat(trashPath,"휴지통");
	if ( (hFile = _findfirsti64("휴지통", &c_file)) == -1L ||  !(c_file.attrib & _A_SUBDIR))
	{
		makeDirectory("휴지통" , currentPath);
	}
	getDirectoryFiles(currentPath);
	_findclose(hFile);
}

void clearResult()
{
	int i, j;
	for(i=1 ; i < 17 ; i++)
	{
		gotoxy(0,i);
		printf("                                                                                                    \n");
	}
}

void printResult(char *string)
{
	int i;
	gotoxy(0,18);
	for(i=18 ; i <= 24 ; i++)
	{
		printf("                                                                                                       ");
	}
	gotoxy(0,22);
	printf("%s",string);
	printCurrentPath();
}

void getFilePathName(char *path, char *filePath, char *file)
{
	struct _finddata_t c_file;
	long hFile;
	char check[255];
	if( (hFile = _findfirst( path, &c_file )) == -1L )
	{
		strcpy(filePath,"");
		strcpy(file, "");
	}
	else
	{
		strcpy(check,currentPath);
		strcat(check,"\\");
		strcat(check, c_file.name);
		if(strcmp(path,currentPath)==0)
		{
			strcpy(filePath,currentPath);
		}
		else if(strcmp(path,c_file.name)==0)
		{
			strcpy(filePath,currentPath);
		}
		else
		{
			strcpy(filePath,path);
		}
		strcpy(file, c_file.name);
	}
}

int selectShowOption(char *option , char  *showWay)
{
	if(strlen(option) != 1)
	{
		return 1;
	}
	else if(strstr(option,"l") !=0 ||
		strstr(option,"w") !=0 )
	{
		*showWay = option[0];
		return 0;
	}
	else
	{
		return 1;
	}
}

void printCurrentPath()
{
	gotoxy(0,18);
	printf("                                                                               ");
	gotoxy(0,18);
	printf("%s>",currentPath);
}

void getCurrentPath()
{
	_getcwd(currentPath, _MAX_PATH);
}

int sort(char *option)
{
	if(strcmp(option, "au") == 0
		||strcmp(option, "ad") == 0
		||strcmp(option, "du") == 0
		||strcmp(option, "dd") == 0
		||strcmp(option, "su") == 0
		||strcmp(option, "sd") == 0)
	{
		strcpy(sortWay, option);
		return 0;
	}
	else
	{
		return 1;
	}
}

int isFileOrDir(char* s) {

	struct _finddatai64_t c_file;
	intptr_t hFile;
	int result;
	if ( (hFile = _findfirsti64(s, &c_file)) == -1L )
		result = -1; // 파일 또는 디렉토리가 없으면 -1 반환
	else
		if (c_file.attrib & _A_SUBDIR)
			result = 0; // 디렉토리면 0 반환
		else
			result = 1; // 그밖의 경우는 "존재하는 파일"이기에 1 반환
	_findclose(hFile);
	return result;
}
