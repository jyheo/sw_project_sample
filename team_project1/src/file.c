#include "common.h"

static pageType page;
static fileType *files;
int fileCount;
extern char currentPath[255];
extern char sortWay[3];

int deleteFile(char *option, char *fileName)
{
	int result;
	if(strlen(option) != 1 && strstr(option,"t") ==0 && strstr(option,"c") ==0)
	{
		return 1;
	}
	if(strstr(option,"t") !=0)
	{
		fileCopy(fileName, trashPath);
		deleteFile( "c", fileName);
	}
	else if(strstr(option,"c") !=0)
	{
		if (unlink(fileName))
		{
			return 2;
		} 
		else
		{
			return 0;
		}
	}
}

int changeFileName(char *oldName, char *newName)
{
	if(isFileOrDir(oldName) != 1)
	{
		return 1;
	}
	if(strstr(newName,"\\") !=0 ||
		strstr(newName,"\/") !=0 ||
		strstr(newName,"\/") !=0 ||
		strstr(newName,"\:") !=0 ||
		strstr(newName,"\*") !=0 ||
		strstr(newName,"\?") !=0 ||
		strstr(newName,"\"") !=0 ||
		strstr(newName,"\<") !=0 ||
		strstr(newName,"\>") !=0 ||
		strstr(newName,"\|") !=0 )
	{
		return 2;
	}
	if(rename(oldName, newName) == 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

void show(findfileType *root, int findFileCount,int position)
{
	char datePrint[100];
	clearResult();
	gotoxy(0,2);
	for(; root; root=root->next)
	{
		if(root->position == position)
		{
			printf("이        름 : %s\n\n",root->file.name);
			printf("위        치 : %s\n\n",root->file.path);
			printf("크        기 : %I64d\n\n",root->file.size);
			printf("속        성 : ");
			if(root->file.attrib)
			{
				printf("FILE\n\n");
			}
			else
			{
				printf("DIR\n\n");
			}
			strcpy(datePrint, ctime( &(root->file.writeTime)));
			datePrint[strlen(datePrint)-2] = NULL;
			printf("최근수정날짜 : %s\n\n",datePrint);
			strcpy(datePrint, ctime( &(root->file.makeTime)));
			datePrint[strlen(datePrint)-2] = NULL;
			printf("만 든 날 짜 : %s\n",datePrint);
			gotoxy(0,16);
			printf("                                    %3d / %3d      (다음페이지 n , 이전페이지 p)\n", position, findFileCount);
			break;
		}
	}
}
void showFindFiles(findfileType *root, int findFileCount)
{
	int position=1;
	char command[255];
	findfileType *temp;
	show(root,findFileCount,position);
	printResult("상세파일 종료를 원하시면 exit를 눌러주시기 바랍니다");
	gets(command);
	while(strcmp(command,"exit") !=0)
	{
		if(strcmp(command,"p") ==0 && position > 1)
		{
			position--;
			show(root,findFileCount,position);
		}
		if(strcmp(command,"n") ==0 && position < findFileCount)
		{
			position++;
			show(root,findFileCount,position);
		}
		printCurrentPath();
		gets(command);
		while(root)
		{
			temp = root;
			root = root->next;
			free(temp);
		}
	}
}

int fileMove(const char* src, const char* dst)
{
	char srcPath[255], srcName[255];
	char dstPath[255], dstName[255];
	getFilePathName(src, srcPath, srcName);
	getFilePathName(dst, dstPath, dstName);
	if(isFileOrDir(srcName) != 1)
	{
		return 1;
	}
	if(isFileOrDir(dstName) != 0)
	{
		return 2;
	}
	strcat(srcPath,"\\");
	strcat(dstPath,"\\");
	strcat(srcPath,srcName);
	strcat(dstPath,dstName);
	if(strcmp(srcPath,dstPath)==0)
	{
		return 3;
	}
	fileCopy(srcPath, dstName);
	deleteFile("c", srcName);
	return 0;
}

int fileCopy(const char* src, const char* dst) {
	FILE *fp_source, *fp_dest;
	char oneByte;
	int i;
	char dest[255]="";
	char srcPath[255], srcName[255];
	char dstPath[255], dstName[255];
	char checkName[255];
	char fileName[245];
	char fileExtension[10];
	char j='0';
	char checkPath[255];
	int len;
	char preName[255];
	fp_source = fopen(src, "rb");
	if( fp_source == NULL )  // 파일열기에 실패했을 때는 b 파일을 열지 않아야겠죠.
	{
		return 1;
	}
	strcpy(dest,"");
	getFilePathName(src, srcPath, srcName);
	getFilePathName(dst, dstPath, dstName);
	strcpy(checkName,srcName);
	strcpy(preName,srcName);
	if(strcmp(srcName,dstName) ==0 && strcmp(srcPath, dstPath) == 0)
	{
		return 4;
	}
	if(strcmp(dstPath,"")==0)
	{
		strcpy(dstPath,dst);
	}
	else if(strcmp(dst,dstPath) !=0)
	{
		for(i=0 ; i<fileCount ; i++)
		{
			if(strcmp(dstName, files[i].name) ==0)
			{
				if(files[i].attrib)
				{
					strcpy(checkName,dstName);
					strcpy(preName,dstName);
					strcat(dstPath,"\\");	
					strcat(dstPath,dstName);	
					strcat(dstPath,"\\");	
					getFilePathName(dst, dstPath, dstName);
					if(strcmp(dstPath,"")==0)
					{
						strcpy(dstPath,dst);
					}
					else
					{
						strcat(dstPath,"\\");
					}
					strcpy(checkPath, dstPath);
					strcat(checkPath, checkName);
					getFilePathName(checkPath,checkPath,srcName);
					while(strcmp(checkPath,"") != 0 && j++ <= '9')
					{
						strcpy(checkName, preName);
						strcpy(fileName , strtok(checkName, "."));
						if(strtok(NULL,"") != 0)
						{
							strcpy(fileExtension, strtok(NULL,""));
						}
						len = strlen(fileName);
						fileName[len++] = '(';
						fileName[len++] = j++;
						fileName[len++] = ')';
						fileName[len++] = NULL;
						strcpy(checkName,fileName);
						strcat(checkName,".");
						strcat(checkName,fileExtension);
						strcpy(checkPath, dstPath);
						strcat(checkPath, checkName);
						getFilePathName(checkPath,checkPath,srcName);
					}
					strcat(dstPath,checkName);	
				}
				else
				{
					strcpy(checkName,srcName);
					strcpy(preName,srcName);
					strcat(dstPath,"\\");
					strcat(dstPath,dstName);
					strcat(dstPath,"\\");
					strcpy(checkPath, dstPath);
					strcat(checkPath, checkName);
					getFilePathName(checkPath,checkPath,srcName);
					while(strcmp(checkPath,"") != 0 && j++ <='9')
					{
						strcpy(checkName, preName);
						strcpy(fileName , strtok(checkName, "."));
						strcpy(fileExtension, strtok(NULL,""));
						len = strlen(fileName);
						fileName[len++] = '(';
						fileName[len++] = j;
						fileName[len++] = ')';
						fileName[len++] = NULL;
						strcpy(checkName,fileName);
						strcat(checkName,".");
						strcat(checkName,fileExtension);
						strcpy(checkPath, dstPath);
						strcat(checkPath, checkName);
						getFilePathName(checkPath,checkPath,srcName);
					}
					strcat(dstPath,checkName);	
				}
				break;
			}
		}
	}
	else if(strcmp(dst,dstPath) ==0)
	{
		strcpy(checkName,srcName);
		strcpy(preName,srcName);
		strcat(dstPath,"\\");
		strcpy(checkPath, dstPath);
		strcat(checkPath, checkName);
		getFilePathName(checkPath,checkPath,srcName);
		while(strcmp(checkPath,"") != 0 && j++ <='9')
		{
			strcpy(checkName, preName);
			strcpy(fileName , strtok(checkName, "."));
			strcpy(fileExtension, strtok(NULL,""));
			len = strlen(fileName);
			fileName[len++] = '(';
			fileName[len++] = j;
			fileName[len++] = ')';
			fileName[len++] = NULL;
			strcpy(checkName,fileName);
			strcat(checkName,".");
			strcat(checkName,fileExtension);
			strcpy(checkPath, dstPath);
			strcat(checkPath, checkName);
			getFilePathName(checkPath,checkPath,srcName);
		}
		strcat(dstPath,checkName);	
	}
	if(j > '9')
	{
		fclose(fp_source);
		return 2;
	}
	fp_dest   = fopen(dstPath, "wb");
	if( fp_dest == NULL )  // 파일열기에 실패했을 때는 b 파일을 열지 않아야겠죠.
	{
		fclose(fp_source);
		return 3;
	}
	while(!feof(fp_source)) {
		oneByte = fgetc(fp_source);
		fputc(oneByte, fp_dest);
	}
	fclose(fp_source);
	fclose(fp_dest);
	return 0;
}

int makeFile(char *src)
{
	FILE *fp_dest;
	char srcPath[255];
	if(isFileOrDir(src) != -1)
	{
		return 1;
	}
	if(strstr(src,"\\") !=0 ||
		strstr(src,"\/") !=0 ||
		strstr(src,"\/") !=0 ||
		strstr(src,"\:") !=0 ||
		strstr(src,"\*") !=0 ||
		strstr(src,"\?") !=0 ||
		strstr(src,"\"") !=0 ||
		strstr(src,"\<") !=0 ||
		strstr(src,"\>") !=0 ||
		strstr(src,"\|") !=0 )
	{
		return 2;
	}
	strcpy(srcPath, currentPath);
	strcat(srcPath, "\\");
	strcat(srcPath, src);
	fp_dest   = fopen(srcPath, "wb");
	if( fp_dest == NULL )  // 파일열기에 실패했을 때는 b 파일을 열지 않아야겠죠.
	{
		return 3;
	}
	else
	{
		fclose(fp_dest);
		return 0;
	}
}

void showFiles_l(int plus_minus)
{
	int i,j;
	int start,end;
	char datePrint[30];
	clearResult();
	gotoxy(0,2);
	page.currentPage += plus_minus;
	if(page.currentPage < 1)
	{
		page.currentPage++;
	}
	if(page.currentPage > page.totalPage)
	{
		page.currentPage--;
	}
	start = (page.currentPage-1)*MAX_FILENUMBER_OF_PAGE_L;
	end = page.currentPage*MAX_FILENUMBER_OF_PAGE_L;
	end > fileCount ? end = fileCount : end ;
	for(i=start ; i < end  ; i++)
	{
		strcpy(datePrint, ctime( &(files[i].writeTime)));
		datePrint[strlen(datePrint)-2] = NULL;
		printf("%s\t",datePrint);
		if(files[i].attrib)
		{
			printf("<FILE>\t");
			printf("%10I64d\t",files[i].size);
		}
		else
		{
			printf("<DIR> \t");
			printf("          \t",files[i].size);
		}
		if(strlen(files[i].name) > 38)
		{
			for(j = 0 ; j < 38 ; j++)
			{
				printf("%c",files[i].name[j]);
			}
			printf("...\n");
		}
		else
		{
			printf("%s\n",files[i].name);
		}
		if(i == end-1)
		{
			gotoxy(0,16);
			printf("                                    %3d / %3d      (다음페이지 n , 이전페이지 p)\n", page.currentPage, page.totalPage);
		}
	}
	printResult("");
	printCurrentPath();
}

void getDirectoryFiles(char *path)
{
	struct _finddata_t c_file;
	long hFile;
	int i=0;
	char search[255];
	fileCount=0;
	strcpy(search,path);
	if(search[strlen(search)-1]!='\\')
	{
		strcat(search,"\\");
	}
	strcat(search,"*.*");
	if( (hFile = _findfirst( search, &c_file )) == -1L )
		printf( "No files in current directory!\n" );
	else
	{
		do
		{ 
			fileCount++;
		}while( _findnext( hFile, &c_file ) == 0 );
		files = (fileType*)malloc(sizeof(fileType)*fileCount);
		hFile = _findfirst( search, &c_file );
		do
		{
			strcpy(files[i].name, c_file.name);
			if(c_file.attrib != 16 && c_file.attrib != 17)
			{
				files[i].attrib =1;
			}
			else
			{
				files[i].attrib = 0;
			}
			files[i].makeTime = c_file.time_create;
			files[i].writeTime = c_file.time_write;
			files[i++].size = c_file.size;
		}while( _findnext( hFile, &c_file ) == 0 );
		page.totalPage = (fileCount / MAX_FILENUMBER_OF_PAGE_L) + 1;
		if(fileCount % MAX_FILENUMBER_OF_PAGE_L == 0) 
		{
			page.totalPage--;
		}
		page.currentPage=1;
		_findclose( hFile );
	}
		if(strcmp(sortWay, "au") == 0)
		{

			sort_name(files, fileCount,1);
			return 0;
		}
		else if(strcmp(sortWay, "ad") == 0)
		{
			sort_name(files, fileCount,0);
			return 0;
		}
		else if(strcmp(sortWay, "du") == 0)
		{
			sort_date(files, 0, fileCount-1, 1);
			return 0;
		}
		else if(strcmp(sortWay, "dd") == 0)
		{
			sort_date(files, 0, fileCount-1, 0);
			return 0;
		}
		else if(strcmp(sortWay, "su") == 0)
		{
			sort_size(files, 0, fileCount-1, 1);
			return 0;
		}
		else if(strcmp(sortWay, "sd") == 0)
		{
			sort_size(files, 0, fileCount-1, 0);
			return 0;
		}
}

void showFiles_s(int plus_minus)
{
	int i,j,m, n;
	int start,end;
	char datePrint[30];
	clearResult();
	page.currentPage += plus_minus;
	if(page.currentPage < 1)
	{
		page.currentPage++;
	}
	if(page.currentPage > page.totalPage)
	{
		page.currentPage--;
	}
	start = (page.currentPage-1)*MAX_FILENUMBER_OF_PAGE_S;
	end = page.currentPage*MAX_FILENUMBER_OF_PAGE_S;
	end > fileCount ? end = fileCount : end ;
	m = 0;
	n = 2;
	for(i=start ; i < end  ; i++)
	{
		gotoxy(m,n++);
		printf("%.20s",files[i].name);
		if(strlen(files[i].name) > 20)
		{
			printf("...");
		}
		gotoxy(m,n++);
		if(files[i].attrib)
		{
			printf("       <FILE>");
		}
		else
		{
			printf("       <DIR> ");
		}
		gotoxy(m,n++);
		strcpy(datePrint, ctime( &(files[i].writeTime)));
		datePrint[strlen(datePrint)-2] = NULL;
		printf("%s",datePrint);
		gotoxy(m,n++);
		printf("%10I64d",files[i].size);
		n -=4;
		m +=25;
		if(m == 100)
		{
			m = 0;
			n +=5;
		}
		if(i == end-1)
		{
			gotoxy(0,16);
			printf("                                    %3d / %3d      (다음페이지 n , 이전페이지 p)\n", page.currentPage, page.totalPage);
		}
	}
	printResult("");
	printCurrentPath();
}

int showFindAllfile(char *fileName, char* filePath)
{
	findfileType * findfiles = NULL;
	char prevPath[255];
	int findFileCount;
	strcpy(prevPath,currentPath);
	findFileCount = searchFile(fileName, filePath, &findfiles);
	getDirectoryFiles(prevPath);
	moveDirectory(prevPath);
	getCurrentPath();
	showFindFiles(findfiles, findFileCount);
}

int searchFile(char *fileName, char* filePath, findfileType **findfiles)
{
	int result;
	int i;
	char srcPath[250], srcName[250];
	char subDirPath[250];
	char prevPath[250];
	struct _finddata_t c_file;
	long hFile;
	char search[255];
	int len = strlen(fileName);
	int findFileCount=0;
	int j;
	fileType file;
	if(filePath[strlen(filePath)-1] !='\\')
	{
		strcat(filePath,"\\");
	}
	strcpy(search,filePath);
	strcat(search,"*.*");
	if( (hFile = _findfirst( search, &c_file )) != -1L )
	{
		do
		{ 
			if (c_file.attrib & _A_SUBDIR && strcmp(c_file.name , ".") != 0 &&  strcmp(c_file.name , "..") != 0 && !( c_file.attrib & _A_HIDDEN ))
			{
				strcpy(subDirPath,filePath);
				strcat(subDirPath,c_file.name);
				findFileCount = searchFile(fileName, subDirPath,*findfiles);
			}
			else if (!(c_file.attrib & _A_SUBDIR) && !( c_file.attrib & _A_HIDDEN ))
			{
				for( j = 0 ; j < len ; j++)
				{
					if(fileName[j] != c_file.name[j])
					{
						break;
					}
					else if( j == len -1)
					{
						file.attrib = c_file.attrib;
						file.makeTime = c_file.time_create;
						strcpy(file.name, c_file.name);
						file.size = c_file.size;
						file.writeTime = c_file.time_write;
						insertFindFile(*findfiles, &file, filePath);
						findFileCount++;
						if(len < 38)
						{
							i = fileCount;
							break;
						}
					}
				}
			}
		}while( _findnext( hFile, &c_file ) == 0 );
	}
	_findclose( hFile );
	return findFileCount;
}

int fileExe(char *src)
{
	char play[1000];
	char srcPath[255], srcName[255], link[100];
	int i,j=0;
	if(isFileOrDir(src) != 1)
	{
		return 2;
	}
	getFilePathName(src, srcPath, srcName);

	if(strcmp(src,srcName) ==0)
	{
		strcat(srcPath,"\\");
		strcat(srcPath,srcName);
	}
	for(i=0;i<255;i++)
	{
		link[j++]=srcName[i];
		if(link[j-1]=='.')
			j=0;
		if(link[j-1]=='\0')
			break;
	}
	if(strcmp(link,"zip")==0)
	{
		strcpy(play,ZIP_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"txt")==0)
	{
		strcpy(play,TXT_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"xls")==0 || strcmp(link,"xlsx")==0 )
	{
		strcpy(play,XLS_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"pptx")==0)
	{
		strcpy(play,PPT_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"hwp")==0)
	{
		strcpy(play,HWP_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"png")==0)
	{
		strcpy(play,PNG_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"doc")==0)
	{
		strcpy(play,ODC_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else if(strcmp(link,"avi")==0)
	{
		strcpy(play,AVI_LINK);
		strcat(play, " ");
		strcat(play,srcPath);
	}
	else
	{
		return 1;
	}
	WinExec(play,SW_SHOW);
	return 0;
}

int fileShowInformation(char *src)
{
	int i, j;
	int len;
	char prevPath[255];
	char srcPath[255], srcName[255];
	int findFileCount = 0;
	findfileType *findfiles = NULL;
	getFilePathName(src, srcPath, srcName);
	if(isFileOrDir(src) != 1)
	{
		return 1;
	}
	strcpy(prevPath,currentPath);
	len = strlen(srcName);
	getDirectoryFiles(srcPath);
	moveDirectory(srcPath);
	getCurrentPath();
	for(i = 0 ; i < fileCount ; i++)
	{
		for( j = 0 ; j < len ; j++)
		{
			if(srcName[j] != files[i].name[j])
			{
				break;
			}
			else if( j == len -1)
			{
				insertFindFile(&findfiles, &files[i], srcPath);
				findFileCount++;
			}
		}
	}
	getDirectoryFiles(prevPath);
	moveDirectory(prevPath);
	getCurrentPath();
	showFindFiles(findfiles, findFileCount);
	showFiles(0);
}

int insertFindFile(findfileType **pptr, fileType *data, char *path)
{
	findfileType *current;//삽입할 노드를 가리키는 포인터  
	findfileType *temp;//새로운 노드 
	temp=(findfileType *)malloc(sizeof(findfileType));
	if(temp) {
		printf("메모리할당 오류\n");
	}
	strcpy(temp->file.name, data->name);
	temp->file.attrib = data->attrib;
	temp->file.makeTime = data->makeTime;
	temp->file.size = data->size;
	temp->file.writeTime = data->writeTime;
	strcpy(temp->file.path,path);
	temp->next = NULL;
	if(*pptr==NULL) 
	{
		temp->position = 1;
		*pptr = temp;
	}
	else
	{
		for(current =*pptr; current->next; current=current->next);
		temp->position = current->position+1;
		current->next=temp;
	}
	return 0;
}
void showFiles(int plus_minus)
{
	if(showWay == 'l')
	{
		showFiles_l(plus_minus);
	}
	else
	{
		showFiles_s(plus_minus);
	}
}