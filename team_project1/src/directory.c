#include "common.h"
#include "Turboc.h"

char currentPath[255];
extern char showWay;
extern char trashPath[255];

int makeDirectory(char *directoryName, char *path)
{
	char parameter[255];
	char check[255];
	int len;
	char j ='0'-1;

	if(strstr(directoryName,"\\") !=0 ||
		strstr(directoryName,"\/") !=0 ||
		strstr(directoryName,"\/") !=0 ||
		strstr(directoryName,"\:") !=0 ||
		strstr(directoryName,"\*") !=0 ||
		strstr(directoryName,"\?") !=0 ||
		strstr(directoryName,"\"") !=0 ||
		strstr(directoryName,"\<") !=0 ||
		strstr(directoryName,"\>") !=0 ||
		strstr(directoryName,"\|") !=0 ||
		strstr(directoryName,"\.") !=0 )
	{
		return 2;
	}
	strcpy(check,directoryName);
	do{
		if( j != '0'-1)
		{
			strcpy(directoryName,check);
			len = strlen(directoryName);
			directoryName[len++] = '(';
			directoryName[len++] = j;
			directoryName[len++] = ')';
			directoryName[len++] = NULL;
		}
		strcpy(parameter, path);
		if(parameter[strlen(parameter)-1] != '\\')
		{
			strcat(parameter, "\\");
		}
		strcat(parameter, directoryName);
		if(mkdir(parameter) == 0)
		{
			return 0;
		}
	}while( j++ <= '9' );
	return 1;
}

int moveDirectory(char *path)
{
	if (chdir(path)) 
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int changeDirectoryName(char *oldName, char *newName)
{
	if(isFileOrDir(oldName) != 0)
	{
		return 1;
	}
	if(strstr(newName,"\\") !=0 ||
		strstr(newName,"\/") !=0 ||
		strstr(newName,"\/") !=0 ||
		strstr(newName,"\:") !=0 ||
		strstr(newName,"\*") !=0 ||
		strstr(newName,"\?") !=0 ||
		strstr(newName,"\"") !=0 ||
		strstr(newName,"\<") !=0 ||
		strstr(newName,"\>") !=0 ||
		strstr(newName,"\|") !=0 ||
		strstr(newName,"\.") !=0 )
	{
		return 2;
	}
	if(rename(oldName, newName) == 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int deleteDirectoryFull(char t_path[],char buf[])
{
	struct _finddata_t c_file;
	long hFile;
	char tmp[255],temp[255];
	char dst[255];
	strcpy(tmp,t_path);
	if(tmp[strlen(tmp)-1]!='\\')
	{
		strcat(tmp,"\\");
	}
	strcat(tmp,buf);
	strcat(tmp,"\\");
	strcpy(temp,tmp);
	strcat(temp,"*.*");
	if( (hFile = _findfirst(temp, &c_file )) == -1L )
	{
		_findclose(hFile);
		return 1;
	}
	if(rmdir(tmp)==-1)
	{
		do
		{
			if(c_file.attrib & _A_SUBDIR) // 폴더일때
			{
				if(strcmp(c_file.name,".") !=0 && strcmp(c_file.name,"..")!=0)
				{
					deleteDirectoryFull(tmp,c_file.name);
				}
			}
			else if(!(c_file.attrib & _A_SUBDIR))
			{
				strcpy(dst,tmp);
				strcat(dst,c_file.name);
				deleteFile("c",dst);
			}
		}while( _findnext( hFile, &c_file ) == 0 );
		rmdir(tmp);
	}
	_findclose( hFile );
	return 0;
}
int directoryCopy(const char* src, const char* dst, char * path)
{
	int result;
	int i;
	char srcPath[255], srcName[255];
	char dstPath[255], dstName[255];
	char subDirPath[255];
	char prevPath[255];
	struct _finddata_t c_file;
	long hFile;
	char search[255];
	char temp[255];
	strcpy(prevPath,currentPath);
	if(isFileOrDir(src) != 0  || isFileOrDir(dst) != 0)
	{
		return 1;
	}
	getFilePathName(src, srcPath, srcName);
	getFilePathName(dst, dstPath, dstName);
	i=0;
	while(dstPath[i] != NULL && srcPath[i] != NULL)
	{
		if(srcPath[i] != dstPath[i])
		{
			break;
		}
		i++;
	}
	if(srcPath[i]== NULL && strcmp(srcName,dstName) ==0)
	{
		return 3;
	}

	if(strcmp(src, srcName) ==0)
	{
		strcat(srcPath ,"\\");
		strcat(srcPath ,srcName);
	}

	if(strcmp(dst, dstName) ==0)
	{
		strcat(dstPath ,"\\");
		strcat(dstPath ,dstName);
	}
	result = makeDirectory(srcName, dstPath);
	if(result == 1)
	{
		moveDirectory(prevPath);
		getCurrentPath();
		return 1;
	}
	strcpy(search,srcPath);
	if(search[strlen(search)-1]!='\\')
	{
		strcat(search,"\\");
	}
	strcat(search,"*.*");
	strcat(dstPath,"\\");
	strcat(dstPath,srcName);
	if( (hFile = _findfirst( search, &c_file )) != -1L )
	{
		do
		{ 
			if (c_file.attrib & _A_SUBDIR && strcmp(c_file.name , ".") != 0 &&  strcmp(c_file.name , "..") != 0)
			{
				strcpy(subDirPath,srcPath);
				strcat(subDirPath,"\\");
				strcat(subDirPath,c_file.name);
				directoryCopy(subDirPath, dstPath, path );
			}
			else if (!(c_file.attrib & _A_SUBDIR))
			{
				strcpy(subDirPath,srcPath);
				strcat(subDirPath,"\\");
				strcat(subDirPath,c_file.name);
				fileCopy(subDirPath,dstPath);
			}
		}while( _findnext( hFile, &c_file ) == 0 );
	}
	_findclose( hFile );
	return 0;
}