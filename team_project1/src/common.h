#ifndef COMMON_H
#define COMMON_H

#include <windows.h>
#include <stdio.h>
#include <stdlib.h> 
#include <direct.h>  
#include <errno.h>
#include <io.h>
#include <string.h>
#include <ctype.h>

//64비트
/*
#define ZIP_LINK "C:\\Program Files (x86)\\ESTsoft\\ALZip\\ALZip.exe"
#define TXT_LINK "notepad.exe"
#define XLS_LINK "C:\\Program Files (x86)\\Microsoft Office\\Office14\\excel.exe"
#define PPT_LINK "C:\\Program Files (x86)\\Microsoft Office\\Office14\\POWERPNT.EXE"
#define HWP_LINK "C:\\Program Files (x86)\\Hnc\\Hwp80\\Hwp.exe"
#define PNG_LINK "C:\\Windows\\System32\\mspaint.exe"
#define ODC_LINK "C:\\Program Files\\Windows NT\\Accessories\\write.exe"
#define AVI_LINK "C:\\Program Files (x86)\\Windows Media Player\\wmplayer.exe"
*/
#define MAX_FILENUMBER_OF_PAGE_L 15
#define MAX_FILENUMBER_OF_PAGE_S 12


//32비트 
#define ZIP_LINK "C:\\Program Files\\ESTsoft\\ALZip\\ALZip.exe"
#define TXT_LINK "notepad.exe"
#define XLS_LINK "C:\\Program Files\\Microsoft Office\\Office14\\excel.exe"
#define PPT_LINK "C:\\Program Files\\Microsoft Office\\Office14\\POWERPNT.EXE"
#define HWP_LINK "C:\\Program Files\\Hnc\\Hwp80\\Hwp.exe"
#define PNG_LINK "C:\\Windows\\System32\\mspaint.exe"
#define ODC_LINK "C:\\Program Files\\Windows NT\\Accessories\\write.exe"
#define AVI_LINK "C:\\Program Files\\Windows Media Player\\wmplayer.exe"
#define CURRENT_TIMEZONE_OFFSET +9


typedef struct fileType
{
	char name[255];
	int attrib;
	int makeTime;
	int writeTime;
	__int64 size;
	char path[255];
}fileType;

typedef struct pageType
{
	int totalPage;
	int currentPage;
}pageType;

typedef struct findfileType
{
	fileType file;
	int position;
	
	struct findfileType *next;
}findfileType;
typedef struct commandType
{
	char *command;
	char *parameter1;
	char *parameter2;
}commandType;

extern char showWay;
extern char currentPath[255];
extern char trashPath[255];
extern char sortWay[3];

int cmp(char *c1, char *c2, int up_down);
void showTotal();

void showPartition();

void searchHelp();

void help();


void swap(fileType *number1, fileType*number2);

void sort_name(fileType list[], int count,int up_down);

int partition_ascending_size(fileType list[], int left, int right);

int partition_descending_size(fileType list[], int left, int right);

void sort_size(fileType list[], int left, int right, int up_down);

int partition_ascending_date(fileType list[], int left, int right);


int partition_descending_date(fileType list[], int left, int right);


void sort_date(fileType list[], int left, int right, int up_down);


void show(findfileType *root, int findFileCount,int position);

void showFindFiles(findfileType *root, int findFileCount);



int fileMove(const char* src, const char* dst);

// 파일 복사 함수의 본체
int fileCopy(const char* src, const char* dst); 
int makeFile(char *src);



void insertion_sort(fileType list[]);
void showFiles_l(int plus_minus);



void getDirectoryFiles(char *path);



void showFiles_s(int plus_minus);


int showFindAllfile(char *fileName, char* filePath);

int searchFile(char *fileName, char* filePath, findfileType **findfiles);


int directoryCopy(const char* src, const char* dst, char * path);


int isFileOrDir(char* s);
int deleteDirectoryFull(char t_path[],char buf[]);


int sort(char *option);


int fileExe(char *src);


//수정 findfileType
int fileShowInformation(char *src);


int insertFindFile(findfileType **pptr, fileType *data, char *path);

void printCurrentPath();

void getCurrentPath();


void showFiles(int plus_minus);

int makeDirectory(char *directoryName, char *path);


int moveDirectory(char *path);


int deleteDirectory(char *directoryName);


int changeDirectoryName(char *oldName, char *newName);


int changeFileName(char *oldName, char *newName);

int deleteFile(char *option, char *fileName);

void start();


void clearResult();

void printResult(char *string);


void setTime(char *country);

void getWorldTime(int tzOffset, char* buffer, size_t bufferSize);
void showTime(char country);



void getFilePathName(char *path, char *filePath, char *file);


int selectShowOption(char *option , char  *showWay);





#endif