#include "common.h"
extern fileCount;

int cmp(char *c1, char *c2, int up_down)
{
	char fileName1[255];
	char fileName2[255];

	strcpy(fileName1,c1);
	strcpy(fileName2,c2);

	strupr(fileName1);
	strupr(fileName2);
	return strcmp(fileName1,fileName2);
}

void swap(fileType *number1, fileType*number2)
{
	fileType temp;
	temp=*number1;
	*number1=*number2;
	*number2=temp;
}

void sort_name(fileType list[], int count,int up_down)
{	int i, j, least;
	fileType temp;
	if(up_down)
	{
	for(i=0; i<count-1; i++)
	{
		least = i;
		for(j=i+1; j<count; j++) 			// �ּҰ� Ž��
		{
			if(cmp(list[j].name, list[least].name, up_down) < 0 )
			{
				least = j;
			}
		}
		swap(&list[i], &list[least]);
	}
	}
	else
	{
		for(i=0; i<count-1; i++)
	{
		least = i;
		for(j=i+1; j<count; j++) 			// �ּҰ� Ž��
		{
			if(cmp(list[j].name, list[least].name, up_down) > 0 )
			{
				least = j;
			}
		}
		swap(&list[i], &list[least]);
	}
	}
}

int partition_ascending_size(fileType list[], int left, int right)
{
	int low, high, pivot;
	pivot = left;
	low = left;
	high = right+1;
	do{
		do{
			low++;
		}while(list[low].size < list[pivot].size && low <= right);
		do{
			high--;
		}while(list[high].size > list[pivot].size && high >= left);

		if(high > low)
		{
			swap(&list[low], &list[high]);
		}
	}while(low < high);

	swap(&list[pivot] , &list[high]);
	return high;
}

int partition_descending_size(fileType list[], int left, int right)
{
	int low, high, pivot;
	pivot = left;
	low = left;
	high = right+1;
	do{
		do{
			low++;
		}while(list[low].size > list[pivot].size && low <= right);
		do{
			high--;
		}while(list[high].size < list[pivot].size && high >= left);

		if(high > low)
		{
			swap(&list[low], &list[high]);
		}
	}while(low < high);

	swap(&list[pivot] , &list[high]);
	return high;
}

void sort_size(fileType list[], int left, int right, int up_down)
{
	if(left < right)
	{
		int q;
		if(up_down)
		{
			q = partition_ascending_size(list, left, right);
		}
		else
		{
			q = partition_descending_size(list, left, right);
		}
		sort_size(list, left, q-1, up_down);
		sort_size(list, q+1, right, up_down);
	}
}

int partition_ascending_date(fileType list[], int left, int right)
{
	int low, high, pivot;
	pivot = left;
	low = left;
	high = right+1;
	do{
		do{
			low++;
		}while(list[low].writeTime < list[pivot].writeTime && low <= right);
		do{
			high--;
		}while(list[high].writeTime > list[pivot].writeTime && high >= left);

		if(high > low)
		{
			swap(&list[low], &list[high]);
		}
	}while(low < high);
	swap(&list[pivot] , &list[high]);
	return high;
}

int partition_descending_date(fileType list[], int left, int right)
{
	int low, high, pivot;
	pivot = left;
	low = left;
	high = right+1;

	do{
		do{
			low++;
		}while(list[low].writeTime > list[pivot].writeTime && low <= right);
		do{
			high--;
		}while(list[high].writeTime < list[pivot].writeTime && high >= left);

		if(high > low)
		{
			swap(&list[low], &list[high]);
		}
	}while(low < high);

	swap(&list[pivot] , &list[high]);
	return high;
}

void sort_date(fileType list[], int left, int right, int up_down)
{
	if(left < right)
	{
		int q;
		if(up_down)
		{
			q = partition_ascending_date(list, left, right);
		}
		else
		{
			q = partition_descending_date(list, left, right);
		}
		sort_date(list, left, q-1, up_down);
		sort_date(list, q+1, right, up_down);

	}
}
