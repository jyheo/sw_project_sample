#include "Turboc.h"
#include "common.h"

char trashPath[255];
char showWay='l';
char country = 'k';
char sortWay[3] = "au";

int main()
{
	char input[255];
	int result;
	commandType command;

	start();
	while(1)
	{
		gets(input);
		if(strcmp(input, "") == 0)
		{
			continue;
		}
		command.command = strtok(input, " ");
		if(strcmp(command.command, "\/help") ==0)
		{
			help();
			getDirectoryFiles(currentPath);
				showFiles(0);
		}
		else if(strcmp(command.command, "p") ==0)
		{
			showFiles(-1);
		}
		else if(strcmp(command.command, "n") ==0)
		{
			showFiles(1);
		}
		else if(strcmp(command.command, "\/settime") ==0)
		{
			setTime(&country);
			showFiles(0);
			printResult("국가를 설정하였습니다.");
		}
		else if(strcmp(command.command, "\/time") ==0)
		{
			showTime(country);
			showFiles(0);
		}
		else if(strcmp(command.command, "\/di") ==0 
			|| strcmp(command.command, "\/dn") ==0
			|| strcmp(command.command, "\/dd") ==0
			|| strcmp(command.command, "\/ds") ==0
			|| strcmp(command.command, "\/dp") ==0
			|| strcmp(command.command, "\/fn") ==0
			|| strcmp(command.command, "\/fv") ==0
			|| strcmp(command.command, "\/fp") ==0)
		{
			command.parameter1 = strtok(NULL,"");
			if(command.parameter1 == NULL)
			{
				printResult("잘못된 명령어입니다.");
				continue;
			}
		}
		else
		{
			command.parameter1 = strtok(NULL," ");
			command.parameter2 = strtok(NULL, "");

			if(command.parameter1 == NULL || command.parameter2 ==NULL)
			{
				printResult("잘못된 명령어입니다.");
				continue;
			}
		}
		if(strcmp(command.command, "\/ds") ==0)
		{
			result = sort(command.parameter1);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("정렬하였습니다.");
				break;
			case 1:
				printResult("정렬할수 없습니다.(명령어 확인요망)");
				break;
			}
		}
		else if(strcmp(command.command, "\/di") ==0)
		{
			result = moveDirectory(command.parameter1);
			switch(result)
			{
			case 0:
				getCurrentPath();
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리를 이동하였습니다.");
				break;
			case 1:
				printResult("존재하지 않는 디렉토리입니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/dp") ==0)
		{
			result = selectShowOption(command.parameter1 , &showWay);
			showFiles(0);
			switch(result)
			{
			case 0:
				printResult("보여주기 방식이 변경되었습니다.");
				break;
			case 1:
				printResult("보여주기 방식을 변경할수 없습니다.(속성확인 요망)");
				break;
			}
		}
		else if(strcmp(command.command, "\/dn") ==0)
		{
			result = makeDirectory(command.parameter1,currentPath);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리를 성공적으로 생성하였습니다.");
				break;
			case 1:
				printResult("디렉토리를 만들수 없습니다.(중복이름 초과)");
				break;
			case 2:
				printResult("디렉토리를 만들수 없습니다.(특수문자 사용) - 사용 불가 문자 \: \₩, \/, \:, \*, \?, \", \<, \>, \|, \.");
				break;
			}
		}
		else if(strcmp(command.command, "\/dd") ==0)
		{
			result = deleteDirectoryFull(currentPath,command.parameter1);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리를 성공적으로 제거하였습니다.");
				break;
			case 1:
				printResult("디렉토리를 제거할수 없습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fd") ==0)
		{
			result = deleteFile(command.parameter2, command.parameter1);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리를 성공적으로 제거하였습니다.");				
				break;
			case 1:
				printResult("디렉토리를 삭제 할 수 없습니다.(파일삭제명령어)");
				break;
			case 2:
				printResult("삭제할 파일이 존재하지 않습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/dcn") ==0)
		{
			result = changeDirectoryName(command.parameter1, command.parameter2);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리명이 성공적으로 변경되었습니다.");
				break;
			case 1:
				printResult("디렉토리명을 변경할수 없습니다.(존재 유무 확인)");
				break;
			case 2:
				printResult("디렉토리명을 변경할수 없습니다.(특수문자 사용) - 사용 불가 문자 \: \₩, \/, \:, \*, \?, \", \<, \>, \|, \.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fcn") ==0)
		{
			result = changeFileName(command.parameter1, command.parameter2);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("파일명을 성공적으로 변경되었습니다.");
				break;
			case 1:
				printResult("파일명을 변경할수 없습니다.(존재 유무 확인)");
				break;
			case 2:
				printResult("파일명을 변경할수 없습니다.(특수문자 사용) - 사용 불가 문자 \: \₩, \/, \:, \*, \?, \", \<, \>, \|");
				break;
			}
		}
		else if(strcmp(command.command, "\/fc") ==0)
		{
			result = fileCopy(command.parameter1, command.parameter2);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("파일을 성공적으로 복사하였습니다.");
				break;
			case 1:
				printResult("파일이 존재하지 않습니다.");
				break;
			case 2:
				printResult("지정한 디렉토리에 파일을 복사할수 없습니다.(파일이름중복 개수 초과)");
				break;
			case 3:
				printResult("디렉토리가 존재하지 않습니다.");
				break;
			case 4:
				printResult("같은 디렉토리에 같은 파일명으로 파일을 복사할수없습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fm") ==0)
		{
			result = fileMove(command.parameter1, command.parameter2);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("파일을 성공적으로 이동하였습니다.");
				break;
			case 1:
				printResult("파일을 이동할수없습니다.(파일존재 확인요망)");
				break;
			case 2:
				printResult("파일을 이동할수없습니다.(디렉토리 확인요망)");
				break;
			case 3:
				printResult("파일을 이동할수없습니다.(같은디렉토리)");
				break;
			}
		}
		else if(strcmp(command.command, "\/dc") ==0)
		{
			result = directoryCopy(command.parameter1, command.parameter2, currentPath);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리를 성공적으로 복사하였습니다..");
				break;
			case 1:
				printResult("디렉토리를 복사할 수 없습니다.(디렉토리 확인요망)");
				break;
			case 2:
				printResult("디렉토리를 복사할 수 없습니다.(중복이름 초과)");
				break;
			case 3:
				printResult("복사할 디렉토리를 하위 디렉토리에 복사할 수 없습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fp") ==0)
		{
			result = fileExe(command.parameter1);			
			switch(result)
			{
			case 0:
				printResult("성공적으로 파일을 실행하였습니다.");
				break;
			case 1:
				printResult("실행 가능한 연결프로그램이 존재하지않습니다.");
				break;
			case 2:
				printResult("파일이 존재하지 않습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fv") ==0)
		{
			result = fileShowInformation(command.parameter1);
			switch(result)
			{
			case 1:
				printResult("파일이 존재하지 않습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fn") ==0)
		{
			result = makeFile(command.parameter1);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("파일을 성공적으로 만들었습니다.");
				break;
			case 1:
				printResult("파일을 만들수 없습니다.(파일존재)");
				break;
			case 2:
				printResult("파일을 만들수 없습니다.(특수문자 사용) - 사용 불가 문자 \: \₩, \/, \:, \*, \?, \", \<, \>, \|");
				break;
			case 3:
				printResult("파일을 만들수 없습니다.(알수없는 이유)");
				break;
			}
		}
		else if(strcmp(command.command, "\/dm") ==0)
		{
			directoryCopy(command.parameter1,command.parameter2,currentPath);
			result = deleteDirectoryFull(currentPath,command.parameter1);
			switch(result)
			{
			case 0:
				getDirectoryFiles(currentPath);
				showFiles(0);
				printResult("디렉토리를 성공적으로 옮겼습니다.");
				break;
			case 1:
				printResult("디렉토리를 옮길수 없습니다.");
				break;
			}
		}
		else if(strcmp(command.command, "\/fs") ==0)
		{
			result = showFindAllfile(command.parameter1, "c:\\");
		}
	}
	return 0;
}