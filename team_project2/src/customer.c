#include "common.h"
#include "customer.h"

extern MYSQL *conn_ptr;

void customer(char *command)
{	
	while(1)
	{
		customerMain();
		inputCommand(command);

		if(strcmp(command,"b") == 0)
		{
			break;
		}
		else if(strcmp(command,"c") == 0)
		{
			joinMember();
		}
		else if(strcmp(command,"r") == 0)
		{
			information(command);
		}
		else if(strcmp(command,"u") == 0)
		{
			update(command);
			//manager(command);
		}
		else if(strcmp(command,"d") == 0)
		{
			drop(command);
			//manager(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}

	}
}

void joinMember()
{
	char query[QUERY_LENGTH];
	member customer;
	char command[COMMAND_LENGTH];

	joinMemberPrint();
	inputCommand(customer.name);
	gotoxy(28,16);
	printf("%s",customer.name);
	inputCommand(customer.birthDay);
	gotoxy(28,22);
	printf("%s",customer.birthDay);
	inputCommand(customer.phone);
	gotoxy(28,29);
	printf("%s",customer.phone);

	gotoxy(10,34);
	printf("회원가입하시겠습니까? (y/n)  ");


	sprintf(query, "insert into customer value "
		"('%s', '%s', '%s', 0)", customer.name,customer.phone,customer.birthDay);

	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			int ok;
			gotoxy(10,36);

			connectDB();
			ok = mysql_query(conn_ptr, query);

			if(!ok)
			{
				printf("회원가입이 완료되었습니다.");
				getch();
			}
			else
			{
				printf("회원가입이 실패하였습니다. %s");
				getch();
			}
			closeDB();
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("회원가입이 취소되었습니다.");
			getch();
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}

void information(char *command)
{
	while(1)
	{
		memberInformationPrint();
		inputCommand(command);
		if(strcmp(command,"b") ==0)
		{
			break;
		}
		else if(strcmp(command,"1") ==0)
		{
			totalMember(command);//디비에서 전체목록을 불러와서 뿌려준다.
			//	break;
		}
		else if(strcmp(command,"2") ==0)
		{
			search(command);
			//	break;
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void printInformation(MYSQL_RES* res, int currentPage)
{
	int i;
	int y=16;
	MYSQL_ROW row;

	for(i = 0 ; i < (currentPage-1)*20 ; i++)
	{
		mysql_fetch_row(res);
	}
	i=0;
	while((row = mysql_fetch_row(res)) != NULL && i < 20)
	{
		gotoxy(10,y);
		printf("%s",row[0]);//10
		gotoxy(24,y);
		printf("%s",row[1]);//10
		gotoxy(39,y);
		printf("%s",row[2]);//10
		gotoxy(61,y++);
		printf("%s",row[3]);//10
		i++;
	}
}

void totalMember(char *command)
{
	char query[QUERY_LENGTH];
	int ok;
	int number;

	int currentPage = 1;
	int maxPage;
	MYSQL_RES* res;
	MYSQL_ROW row;

	sprintf(query, "select * from customer");

	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	number = mysql_num_rows(res);
	maxPage = number % 20 == 0 ? number/20 : (number/20) + 1 ;

	if(number)
	{
		while(1)
		{
			totalMemberPrint();
			gotoxy(1,38);
			printf("│                                                                            이전페이지[p] / 다음페이지 [n] (%d/%d)      │\n",currentPage, maxPage);//3
			printInformation(res, currentPage);
			mysql_free_result(res);
			closeDB();
			connectDB();
			mysql_query(conn_ptr, query);
			res = mysql_store_result(conn_ptr);

			inputCommand(command);
			if(strcmp(command,"b") ==0)
			{
				break;
			}
			else if(strcmp(command,"n") == 0)
			{
				//다음페이지
				if( currentPage < maxPage)
				{
					currentPage++;
				}	
			}
			else if(strcmp(command,"p") == 0)
			{
				//이전페이지
				if( currentPage > 1)
				{
					currentPage--;
				}	
			}
			else
			{
				errorPrint("");
				continue;

			}
		}
	}
	else
	{
		noMemberPrint();
		getch();

	}
	closeDB();
}

void search(char *command)
{
	char search[SEARCH_LENGTH];
	char mode[COMMAND_LENGTH];
	int currentPage = 1;
	int maxPage;
	char query[QUERY_LENGTH];
	int ok;
	int number;
	MYSQL_RES* res;
	MYSQL_ROW row;

	while(1)
	{
		selectSearchMemberPrint();
		inputCommand(command);
		if(strcmp(command,"b") ==0)
		{
			break;
		}
		else if(strcmp(command,"1") ==0 ||
			strcmp(command,"2") ==0)
		{
			

			strcpy(mode, command);


			if(strcmp(command,"b") ==0 || checkTotalMenu(command))
			{
				continue;
			}
			if(strcmp(mode,"1") ==0 )
			{
				nameSearchPrint();
				inputCommand(command);
				sprintf(query, "select * from customer where name='%s'",command);
			}
			else
			{
				phoneSearchPrint();
				inputCommand(command);
				sprintf(query, "select * from customer where phone='%s'",command);
			}
			strcpy(search, command);
			connectDB();
			mysql_query(conn_ptr, query);
			res = mysql_store_result(conn_ptr);
			number = mysql_num_rows(res);
			maxPage = number % 20 == 0 ? number/20 : (number/20) + 1 ;
			if(number)
			{
				while(1)
				{
					searchResultPrint();
					gotoxy(10,12);
					printf("▶ '%s'(으)로 찾은 검색 결과입니다.",search);
					gotoxy(1,38);
					printf("│                                                                            이전페이지[p] / 다음페이지 [n] (%d/%d)      │\n",currentPage, maxPage);//3
					printInformation(res, currentPage);
					mysql_free_result(res);
					closeDB();
					connectDB();
					mysql_query(conn_ptr, query);
					res = mysql_store_result(conn_ptr);

					inputCommand(command);
					if(strcmp(command,"b") ==0)
					{
						break;
					}
					else if(strcmp(command,"n") == 0)
					{
						//다음페이지
						if( currentPage < maxPage)
						{
							currentPage++;
						}	
					}
					else if(strcmp(command,"p") == 0)
					{
						//이전페이지
						if( currentPage > 1)
						{
							currentPage--;
						}	
					}
					else
					{
						errorPrint("");
						continue;

					}
				}
			}
			else
			{
				noMemberPrint();
				getch();
			}
			closeDB();
			//	break;
		}

		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}	
}

void update(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;


	char query[QUERY_LENGTH];
	int ok;
	char phone[PHONE_LENGTH];


	updateMemberPrint();
	inputCommand(command);
	if(strcmp(command,"b") ==0 || checkTotalMenu(command))
	{
		return ;
	}
	sprintf(query, "select *from customer where phone = '%s'",command);

	connectDB();
	ok = mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	ok = mysql_num_rows(res);
	row = mysql_fetch_row(res);
	mysql_free_result(res);
	closeDB();
	if(ok)
	{
		changeInformationPrint();
		gotoxy(28,16);
		printf("%s",row[0]);
		gotoxy(28,22);
		printf("%s",row[2]);
		inputCommand(command);
		gotoxy(28,29);
		printf("%s",command);
		strcpy(phone,command);
		gotoxy(10,34);
		printf("회원정보를 수정하시겠습니까? (y/n)  ");

		while(1)
		{
			inputCommand(command);
			if(strcmp(command,"y") == 0 ||
				strcmp(command,"Y") == 0)
			{
				sprintf(query, "update customer set phone ='%s' where phone = '%s'",phone,row[1]);
				connectDB();
				ok = mysql_query(conn_ptr, query);
				gotoxy(10,36);
				printf("회원정보가 정상적으로 수정되었습니다.");
				getch();
				mysql_free_result(res);
				closeDB();
				break;
			}	
			else if(strcmp(command,"n") == 0 ||
				strcmp(command,"N") == 0)
			{
				gotoxy(10,36);
				printf("회원정보 수정을 취소하였습니다.");
				getch();
				break;
			}
			else
			{
				errorPrint("");			
			}
		}

	}
	else
	{
		gotoxy(10,16);
		printf("※ 존재하지 않는 회원입니다.");
		getch();

	}
}


void drop(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;

	char query[QUERY_LENGTH];
	int ok;
	char phone[PHONE_LENGTH];

	dropMemberPrint();
	inputCommand(command);
	if(strcmp(command,"b") ==0 || checkTotalMenu(command))
	{
		return ;
	}
	sprintf(query, "select *from customer where phone = '%s'",command);
	strcpy(phone,command);
	connectDB();
	ok = mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	ok = mysql_num_rows(res);
	closeDB();
	if(ok)
	{
		gotoxy(10,16);
		printf("회원을 탈퇴 하시겠습니까? (y/n) ");
		while(1)
		{
			inputCommand(command);
			if(strcmp(command,"y") == 0 ||
				strcmp(command,"Y") == 0)
			{

				sprintf(query, "delete from customer where phone = '%s'",phone);
				//sprintf(query, "delete from customer where phone='%s'",phone);
				connectDB();
				ok = mysql_query(conn_ptr, query);
				gotoxy(10,36);
				printf("회원을 정상적으로 탈퇴하였습니다.");
				getch();
				closeDB();
				break;
			}	
			else if(strcmp(command,"n") == 0 ||
				strcmp(command,"N") == 0)
			{
				gotoxy(10,36);
				printf("회원탈퇴를 취소하였습니다.");
				getch();
				break;
			}
			else
			{
				errorPrint("");			
			}
		}
	}
	else
	{
		gotoxy(10,16);
		printf("※ 존재하지 않는 회원입니다.");
		getch();
	}
}

