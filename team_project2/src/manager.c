#include "common.h"
#include "manager.h"

void manager(char *command)
{
	while(1)
	{
		managerMain();
		inputCommand(command);

		if(strcmp(command,"b") == 0)
		{
			break;
		}
		else if(strcmp(command,"t") == 0)
		{
			//테이블설정
			tableSetup(command);
		}
		else if(strcmp(command,"m") == 0)
		{
			//메뉴설정
			menuSetup(command);
		}
		else if(strcmp(command,"a") == 0)
		{
			//회계 내역
			acountSetup(command);
		}
		else if(strcmp(command,"q") == 0)
		{
			//초기화
			initialization(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void tableSetup(char *command)
{
	while(1)
	{
		tableSetupPrint();
		inputCommand(command);

		if(strcmp(command,"b") == 0)
		{
			break;
		}
		else if(strcmp(command,"c") == 0)
		{
			//테이블 추가
			createTable(command);
		}
		else if(strcmp(command,"r") == 0)
		{
			//테이블 정보
			tableInformation(command);
		}
		else if(strcmp(command,"u") == 0)
		{
			//테이블 수정
			updateTable(command);
		}
		else if(strcmp(command,"d") == 0)
		{
			//테이블 삭제
			deleteTable(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void tableInformation(char *command)
{
	char query[QUERY_LENGTH];
	int ok;
	int number;

	int currentPage = 1;
	int maxPage;
	MYSQL_RES* res;
	MYSQL_ROW row;

	sprintf(query, "select * from tables");

	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	number = mysql_num_rows(res);
	maxPage = number % 9 == 0 ? number/9 : (number/9) + 1 ;

	if(number)
	{
		while(1)
		{
			tableInformationPrint(res, currentPage);
			gotoxy(1,38);
			printf("│                                                                            이전페이지[p] / 다음페이지 [n] (%d/%d)      │\n",currentPage, maxPage);
			mysql_free_result(res);
			closeDB();
			connectDB();
			mysql_query(conn_ptr, query);
			res = mysql_store_result(conn_ptr);

			inputCommand(command);
			if(strcmp(command,"b") ==0)
			{
				break;
			}
			else if(strcmp(command,"n") == 0)
			{
				if( currentPage < maxPage)
				{
					currentPage++;
				}	
			}
			else if(strcmp(command,"p") == 0)
			{
				if( currentPage > 1)
				{
					currentPage--;
				}	
			}
			else
			{
				errorPrint("");
				continue;

			}
		}
	}
	else
	{
		notablePrint();
		getch();
	}

	closeDB();

}

void createTable(char *command)
{
	char seq[3];
	char maxPeople[3];
	char option1[PHONE_LENGTH];
	char option2[PHONE_LENGTH];
	char query[QUERY_LENGTH];

	createTablePrint();

	inputCommand(seq);
	gotoxy(30,16);
	printf("%s",seq);
	inputCommand(maxPeople);
	gotoxy(30,20);
	printf("%s",maxPeople);
	inputCommand(option1);
	gotoxy(30,24);
	printf("%s",option1);
	inputCommand(option2);
	gotoxy(30,29);
	printf("%s",option2);
	strcat(option1, " / ");
	strcat(option1, option2);

	gotoxy(10,34);
	printf("테이블을 추가하시겠습니까? (y/n)  ");

	sprintf(query, "insert into tables value "
		"('%s', '%s', '%s')", seq,option1,maxPeople);

	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			int ok;
			gotoxy(10,36);

			connectDB();
			ok = mysql_query(conn_ptr, query);
			closeDB();
			if(!ok)
			{
				
			printf("테이블이 추가되었습니다.");
			tableInit();
			getch();
			}
			else
			{
				printf("테이블 추가를 실패하였습니다. (존재하는 테이블번호)");
				getch();
			}
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("테이블 추가를 취소하였습니다.");
			getch();
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}

void deleteTable(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;

	char query[QUERY_LENGTH];
	int ok;
	char seq[3];


	deleteTablePrint();

	inputCommand(command);
	if(strcmp(command,"b") ==0 || checkTotalMenu(command))
	{
		return ;
	}

	sprintf(query, "select *from tables where seq = '%s'",command);
	strcpy(seq,command);
	connectDB();
	ok = mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	ok = mysql_num_rows(res);
	closeDB();

	if(ok)
	{

		gotoxy(40,16);
		printf("%s",command);

		gotoxy(10,34);
		printf("테이블을 삭제하시겠습니까? (y/n)  ");

		while(1)
		{
			inputCommand(command);
			if(strcmp(command,"y") == 0 ||
				strcmp(command,"Y") == 0)
			{
				sprintf(query, "delete from tables where seq = '%s'",seq);
				
				connectDB();
				ok = mysql_query(conn_ptr, query);
				gotoxy(10,36);
				printf("테이블이 삭제되었습니다.");
				getch();
				closeDB();
				tableInit();
				break;
			}	
			else if(strcmp(command,"n") == 0 ||
				strcmp(command,"N") == 0)
			{
				gotoxy(10,36);
				printf("테이블 삭제를 취소하였습니다.");
				getch();
				break;
			}
			else
			{
				errorPrint("");			
			}
		}
	}
	else
	{
		gotoxy(10,16);
		printf("※ 존재하지 않는 테이블입니다.");
		getch();
	}
}

void updateTable(char *command)
{
	char prevseq[3];
	char seq[3];
	char maxPeople[3];
	char option1[PHONE_LENGTH];
	char option2[PHONE_LENGTH];
	MYSQL_RES* res;
	MYSQL_ROW row;

	char query[QUERY_LENGTH];
	int ok;
	char phone[PHONE_LENGTH];


	updateTablePrint();
	inputCommand(command);
	if(strcmp(command,"b") ==0 || checkTotalMenu(command))
	{
		return ;
	}
	sprintf(query, "select *from tables where seq = '%s'",command);
	strcpy(prevseq,command);
	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	ok = mysql_num_rows(res);
	row = mysql_fetch_row(res);
	mysql_free_result(res);
	closeDB();

	if(ok)
	{
		gotoxy(40,16);
		printf("%s",command);
		gotoxy(8,19);
		printf(" ▶ 테 이 블 번 호 :");
		inputCommand(seq);
		gotoxy(30,19);
		printf("%s",seq);
		gotoxy(8,22);
		printf(" ▶ 가 용 인 원 수 :");
		inputCommand(maxPeople);
		gotoxy(30,22);
		printf("%s",maxPeople);
		gotoxy(8,25);
		printf(" ▶ 테 이 블 종 류 :");
		gotoxy(10,26);
		printf("(다다미 / 테이블)");
		inputCommand(option1);
		gotoxy(30,25);
		printf("%s",option1);
		gotoxy(8,28);
		printf(" ▶ 흡  연  여  부 :");
		gotoxy(10,29);
		printf("(흡 연 / 금 연)");
		inputCommand(option2);
		gotoxy(30,28);
		printf("%s",option2);
		strcat(option1, " / ");
		strcat(option1, option2);

		gotoxy(10,34);
		printf("테이블을 수정하시겠습니까? (y/n)  ");

		while(1)
		{
			inputCommand(command);
			if(strcmp(command,"y") == 0 ||
				strcmp(command,"Y") == 0)
			{
				sprintf(query, "update tables set seq='%s', function='%s', possible_number='%s' where seq = '%s'",seq,option1,maxPeople,prevseq);
				connectDB();
				ok = mysql_query(conn_ptr, query);
				gotoxy(10,36);
				printf("테이블이 수정되었습니다.");
				getch();
				mysql_free_result(res);
				closeDB();
				tableInit();
				break;
			}	
			else if(strcmp(command,"n") == 0 ||
				strcmp(command,"N") == 0)
			{
				gotoxy(10,36);
				printf("테이블 수정을 취소하였습니다.");
				getch();
				break;
			}
			else
			{
				errorPrint("");			
			}
		}
	}
	else
	{
		gotoxy(10,16);
		printf("※ 존재하지 않는 테이블입니다.");
		getch();

	}
}


void menuSetup(char *command)
{
	while(1)
	{
		menuSetupPrint();
		inputCommand(command);

		if(strcmp(command,"b") == 0)
		{
			break;
		}
		else if(strcmp(command,"c") == 0)
		{
			//메뉴 추가
			addMenu(command);
		}
		else if(strcmp(command,"r") == 0)
		{
			//메뉴 정보
			menuInformation(command);
		}
		else if(strcmp(command,"u") == 0)
		{
			//메뉴 수정
			updateMenu(command);
		}
		else if(strcmp(command,"d") == 0)
		{
			//메뉴 삭제
			dropMenu(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void dropMenu(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;

	char query[QUERY_LENGTH];
	int ok;
	


	deleteMenuPrint();

	inputCommand(command);
	if(strcmp(command,"b") ==0 || checkTotalMenu(command))
	{
		return ;
	}

	sprintf(query, "select *from menu where name = '%s'",command);
	connectDB();
	ok = mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	ok = mysql_num_rows(res);
	closeDB();

	if(ok)
	{

		gotoxy(40,16);
		printf("%s",command);

		gotoxy(10,34);
		printf("메뉴를 삭제하시겠습니까? (y/n)  ");
		sprintf(query, "delete from menu where name = '%s'",command);

		while(1)
		{
			inputCommand(command);
			if(strcmp(command,"y") == 0 ||
				strcmp(command,"Y") == 0)
			{
				
				
				connectDB();
				ok = mysql_query(conn_ptr, query);
				gotoxy(10,36);
				printf("메뉴가 삭제되었습니다.");
				
				getch();

				closeDB();
				menuInit();
				break;
			}	
			else if(strcmp(command,"n") == 0 ||
				strcmp(command,"N") == 0)
			{
				gotoxy(10,36);
				printf("메뉴 삭제를 취소하였습니다.");
				getch();
				break;
			}
			else
			{
				errorPrint("");			
			}
		}
	}
	else
	{
		gotoxy(10,16);
		printf("※ 존재하지 않는 메뉴입니다.");
		getch();
	}
}

void updateMenu(char *command)
{
	char menuName[MENU_NAME_LENGTH];
	menuType changeMenu;
	char query[QUERY_LENGTH];
	MYSQL_RES* res;
	MYSQL_ROW row;
	int number;

	int ok;


	updateMenuPrint();
	inputCommand(command);
	if(strcmp(command,"b") ==0 || checkTotalMenu(command))
	{
		return ;
	}
	sprintf(query, "select *from menu where name = '%s'",command);
	strcpy(menuName,command);
	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	ok = mysql_num_rows(res);
	row = mysql_fetch_row(res);
	mysql_free_result(res);
	closeDB();

	if(ok)
	{
		gotoxy(40,16);
		printf("%s",command);
		gotoxy(8,19);
		printf(" ▶  이        름  :");
		inputCommand(changeMenu.name);
		gotoxy(30,19);
		printf("%s",changeMenu.name);
		gotoxy(8,22);
		printf(" ▶  코        드  :");
		inputCommand(changeMenu.code);
		gotoxy(30,22);
		printf("%s",changeMenu.code);
		gotoxy(8,25);
		printf(" ▶  분        류  :");
		gotoxy(10,26);
		printf("(안주, 주류, 음료)");
		inputCommand(changeMenu.group);
		gotoxy(30,25);
		printf("%s",changeMenu.group);
		gotoxy(8,28);
		printf(" ▶  가        격  :");
		inputCommand(command);
		gotoxy(30,28);
		printf("%s",command);

		gotoxy(10,34);
		printf("메뉴를 수정하시겠습니까? (y/n)  ");
		sprintf(query, "update menu set name='%s', code='%s', sort='%s', price='%s' where name = '%s'",changeMenu.name,changeMenu.code,changeMenu.group,command,menuName);
		while(1)
		{
			inputCommand(command);
			if(strcmp(command,"y") == 0 ||
				strcmp(command,"Y") == 0)
			{
				
				connectDB();
				mysql_query(conn_ptr, query);
				gotoxy(10,36);
				printf("메뉴 정보가 수정되었습니다.");
				getch();
				closeDB();
				menuInit();
				break;
			}	
			else if(strcmp(command,"n") == 0 ||
				strcmp(command,"N") == 0)
			{
				gotoxy(10,36);
				printf("메뉴 수정을 취소하였습니다.");
				getch();
				break;
			}
			else
			{
				errorPrint("");			
			}
		}
	}
	else
	{
		gotoxy(10,16);
		printf("※ 존재하지 않는 메뉴입니다.");
		getch();

	}
}



void addMenu(char *command)
{
	menuType newMenu;
	char query[QUERY_LENGTH];

	addMenuPrint();

	inputCommand(newMenu.name);
	gotoxy(26,16);
	printf("%s",newMenu.name);
	inputCommand(newMenu.code);
	gotoxy(26,20);
	printf("%s",newMenu.code);
	inputCommand(newMenu.group);
	gotoxy(26,24);
	printf("%s",newMenu.group);
	inputCommand(command);
	gotoxy(26,29);
	printf("%s",command);


	gotoxy(10,34);
	printf("메뉴를 추가하시겠습니까? (y/n)  ");

	sprintf(query, "insert into menu value "
		"('%s', '%s', '%s', '%s')", newMenu.name,command,newMenu.group,newMenu.code);

	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			int ok;
			gotoxy(10,36);

			connectDB();
			ok = mysql_query(conn_ptr, query);
			closeDB();
			if(!ok)
			{
				printf("메뉴가 추가되었습니다.");
				menuInit();
				getch();
			}
			else
			{
				printf("메뉴 추가를 실패하였습니다. (존재하는 메뉴) %s",query);
				getch();
			}
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("메뉴 추가를 취소하였습니다.");
			getch();
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}

void menuInformation(char *command)
{
	char query[QUERY_LENGTH];
	int ok;
	int number;
	menuPType menuP = {0,};
	int currentPage = 1;
	int maxPage;
	MYSQL_RES* res;
	MYSQL_ROW row;

	sprintf(query, "select * from menu");

	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	number = mysql_num_rows(res);
	//maxPage = number % 9 == 0 ? number/9 : (number/9) + 1 ;

	if(number)
	{
		while(1)
		{
			menuInformationPrint(res);
			detailMenuPrint(res, &menuP);
			
			//gotoxy(1,38);
			//printf("│                                                                            이전페이지[p] / 다음페이지 [n] (%d/%d)      │\n",currentPage, maxPage);
			mysql_free_result(res);
			closeDB();
			connectDB();
			mysql_query(conn_ptr, query);
			res = mysql_store_result(conn_ptr);

			inputCommand(command);
			if(strcmp(command,"b") ==0)
			{
				break;
			}
			/*else if(strcmp(command,"n") == 0)
			{
				if( currentPage < maxPage)
				{
					currentPage++;
				}	
			}
			else if(strcmp(command,"p") == 0)
			{
				if( currentPage > 1)
				{
					currentPage--;
				}	
			}*/
			else
			{
				errorPrint("");
				continue;

			}
		}
	}
	else
	{
		noMenuPrint();
		getch();
	}

	closeDB();

}

void acountSetup(char *command)
{
	while(1)
	{
		acountSetupPrint();
		inputCommand(command);

		if(strcmp(command,"b") == 0)
		{
			break;
		}
		else if(strcmp(command,"d") == 0)
		{
			//일간 매출
			dayAmount(command);
		}
		else if(strcmp(command,"m") == 0)
		{
			//월간 매출
			monthAmount(command);
		}
		else if(strcmp(command,"y") == 0)
		{
			//년간 매출
			yearAmount(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void dayAmount(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;
	int result=0;
	char query[QUERY_LENGTH];
	int ok;
	dateType date;

	amountPrint("日");
	
	gotoxy(10, 16);
	printf("▶  年를 입력하세요 : ");
	inputCommand(date.year);
	gotoxy(33, 16);
	printf("%s",date.year);
	gotoxy(10, 18);
	printf("▶  月를 입력하세요 : ");
	inputCommand(date.month);
	gotoxy(33, 18);
	printf("%s",date.month);
	gotoxy(10, 20);
	printf("▶  日을 입력하세요 : ");
	inputCommand(date.day);
	gotoxy(33, 20);
	printf("%s",date.day);

	sprintf(query,"select price from amount where year = '%s' and month = '%s' and day = '%s' ",date.year, date.month, date.day);
	connectDB();
	ok = mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);

	while((row=mysql_fetch_row(res)) != NULL)
	{
		result += atoi(row[0]);
	}

	gotoxy(10,36);
	printf("%s년 %s월 %s일의 매출은 %d원 입니다.", date.year, date.month, date.day, result);
	getch();
	mysql_free_result(res);
	closeDB();

}
void monthAmount(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;
	int result=0;
	char query[QUERY_LENGTH];
	dateType date;

	amountPrint("月");
	
	gotoxy(10, 16);
	printf("▶  年를 입력하세요 : ");
	inputCommand(date.year);
	gotoxy(33, 16);
	printf("%s",date.year);
	gotoxy(10, 18);
	printf("▶  月를 입력하세요 : ");
	inputCommand(date.month);
	gotoxy(33, 18);
	printf("%s",date.month);

	//해당 월의 값
	sprintf(query,"select price from amount where year = '%s' and month = '%s'",date.year, date.month);
	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);

	while((row=mysql_fetch_row(res)) != NULL)
	{
		result += atoi(row[0]);
	}

	gotoxy(10,36);
	printf("%s년 %s월의 매출은 %d원 입니다.", date.year, date.month, result);
	getch();
	mysql_free_result(res);
	closeDB();
}
void yearAmount(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;
	int result=0;
	char query[QUERY_LENGTH];
	dateType date;

	amountPrint("年");
	
	gotoxy(10, 16);
	printf("▶  年를 입력하세요 : ");
	inputCommand(date.year);
	gotoxy(33, 16);
	printf("%s",date.year);

	//해당연의 모든 쿼리불러와서 값을더한다.

	sprintf(query,"select price from amount where year = '%s'",date.year);
	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);

	while((row=mysql_fetch_row(res)) != NULL)
	{
		result += atoi(row[0]);
	}

	gotoxy(10,36);
	printf("%s년의 매출은 %d원 입니다.", date.year, result);
	getch();
	mysql_free_result(res);
	closeDB();

}


void initialization(char *command)
{
	while(1)
	{
		initializationPrint();
		inputCommand(command);

		if(strcmp(command,"b") == 0)
		{
			break;
		}
		else if(strcmp(command,"t") == 0)
		{
			//테이블 초기화
			tableReset(command);
		}
		else if(strcmp(command,"i") == 0)
		{
			//회원정보 초기화
			customerReset(command);
		}
		else if(strcmp(command,"m") == 0)
		{
			//메뉴 초기화
			menuReset(command);
		}
		else if(strcmp(command,"a") == 0)
		{
			//전체 초기화
			allReset(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}
void tableReset(char *command)
{
	resetPrint("테이블");
	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			connectDB();
			mysql_query(conn_ptr, "delete from tables");
			closeDB();

			gotoxy(10,36);
			printf("테이블이 초기화되었습니다.");
			getch();
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("테이블 초기화를 취소하였습니다.");
			getch();
			break;
		}
		else if(strcmp(command,"b") == 0 )
		{
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}
void customerReset(char *command)
{
	resetPrint("고객정보");
	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			connectDB();
			mysql_query(conn_ptr, "delete from customer");
			closeDB();

			gotoxy(10,36);
			printf("고객정보가 초기화되었습니다.");
			getch();
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("고객정보 초기화를 취소하였습니다.");
			getch();
			break;
		}
		else if(strcmp(command,"b") == 0 )
		{
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}
void menuReset(char *command)
{
	resetPrint("메뉴");
	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			connectDB();
			mysql_query(conn_ptr, "delete from menu");
			closeDB();

			gotoxy(10,36);
			printf("메뉴가 초기화되었습니다.");
			getch();
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("메뉴 초기화를 취소하였습니다.");
			getch();
			break;
		}
		else if(strcmp(command,"b") == 0 )
		{
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}
void allReset(char *command)
{
	resetPrint("전체");
	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			connectDB();
			mysql_query(conn_ptr, "delete from tables");
			closeDB();
			connectDB();
			mysql_query(conn_ptr, "delete from customer");
			closeDB();
			connectDB();
			mysql_query(conn_ptr, "delete from menu");
			closeDB();

			gotoxy(10,36);
			printf("전체가 초기화되었습니다.");
			getch();
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("전체 초기화를 취소하였습니다.");
			getch();
			break;
		}
		else if(strcmp(command,"b") == 0 )
		{
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}