#include <windows.h> //윈도우즈와 관련된 것을 처리해주는 입출력 함수입니다.
gotoxy(int x, int y)//내가 원하는 위치로 커서 이동
{
	COORD pos = {x-1, y-1};//커서가 X좌표에서 -1 한값. Y좌표에서 -1한 값으로 이동
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);// WIN32API 함수입니다. 이건 알필요 없어요
}