#include "common.h"
#include "state.h"
#include "menu.h"

tableType *tables;
menuTree *menus= NULL;
int totalTableNumber=0;
void state(char *command)
{
	commandType stateCommand;
	int currentPage = 1;
	int maxPage = totalTableNumber % 9 == 0 ? totalTableNumber/9 : (totalTableNumber/9) + 1 ;


	while(1)
	{
		stateMain(tables, currentPage, totalTableNumber);
		inputCommand(command);
		if(strcmp(command,"") == 0)
		{
			continue;
		}

		stateCommand.command = strtok(command, " ");

		if(strcmp(stateCommand.command,"b") == 0)
		{
			break;
		}
		else if(strcmp(stateCommand.command,"o") == 0)
		{
			//메뉴주문

			int tableNumber;
			stateCommand.parameter1 = strtok(NULL,"");
			if(stateCommand.parameter1 == NULL)
			{
				errorPrint("");
				continue;
			}

			tableNumber = atoi(stateCommand.parameter1);
			if(tableNumber<1 || tableNumber > totalTableNumber)
			{
				errorPrint("(테이블이 존재하지 않습니다.)");
			}
			else if(tables[tableNumber-1].currentPeople == 0 )
			{
				errorPrint("(테이블이 비어있습니다.)");
			}
			else
			{
				order(command, tableNumber);
			}
		}
		else if(strcmp(stateCommand.command,"c") == 0)
		{
			int tableNumber;
			stateCommand.parameter1 = strtok(NULL,"");
			if(stateCommand.parameter1 == NULL)
			{
				errorPrint("");
				continue;
			}

			tableNumber = atoi(stateCommand.parameter1);
			if(tableNumber<1 || tableNumber > totalTableNumber)
			{
				errorPrint("(테이블이 존재하지 않습니다.)");
			}
			else if(tables[tableNumber-1].currentPeople == 0 )
			{
				errorPrint("(테이블이 비어있습니다.)");
			}
			else
			{
				cancle(command, tableNumber);
			}
		}
		else if(strcmp(stateCommand.command,"h") == 0)
		{
			int tableNumber;
			//헌팅 설정
			stateCommand.parameter1 = strtok(NULL,"");
			if(stateCommand.parameter1 == NULL)
			{
				errorPrint("");
				continue;
			}
			tableNumber = atoi(stateCommand.parameter1);
			haunting(stateCommand.command, tableNumber);
		}
		else if(strcmp(stateCommand.command,"t") == 0)
		{
			//테이블 합치기
			int to, from;
			stateCommand.parameter1 = strtok(NULL," ");
			stateCommand.parameter2 = strtok(NULL, "");


			if(stateCommand.parameter1 == NULL || stateCommand.parameter2 ==NULL)
			{
				errorPrint("");
				continue;
			}
			from = atoi(stateCommand.parameter1);
			to = atoi(stateCommand.parameter2);
			tableAdd(command, to, from);
		}
		else if(strcmp(stateCommand.command,"n") == 0)
		{
			//다음페이지
			if( currentPage < maxPage)
			{
				currentPage++;
			}	
		}
		else if(strcmp(stateCommand.command,"p") == 0)
		{
			//이전페이지
			if( currentPage > 1)
			{
				currentPage--;
			}	
		}
		else if(strcmp(stateCommand.command,"r") == 0)
		{
			//예약
			reservation(command);
		}
		else if(!checkTotalMenu(command) && atoi(stateCommand.command) == 0)
		{
			errorPrint("");
		}
		else if(checkTotalMenu(command))
		{
			break;
		}
		else
		{
			int tableNumber = atoi(stateCommand.command);

			if(tableNumber <=0 || tableNumber > totalTableNumber )
			{
				errorPrint("(테이블이 존재하지 않습니다.)");
				continue;
			}
			else
			{

				if(tables[tableNumber-1].currentPeople == 0 )
				{
					// 인원수추가하는고
					settingTable(command, tableNumber); 
				}
				else
				{
					//상세매뉴로 들어가는것 계산등
					detailAcount(command, tableNumber);
				}
			}
		}
	}
}
void detailAcount(char *command, int tableNumber)
{
	int sail = 0;
	int point = 0;
	int number;
	char query[QUERY_LENGTH];
	MYSQL_RES* res;
	MYSQL_ROW row;
	char phone[PHONE_LENGTH] = {0,};
	int checkPoint;
	int ok;

	commandType detatilCommand;

	while(1)
	{	
		menuDetailInformationPrint();
		{
			menuPType menuP = {0,};
			printdetailTableInfomation(tables[tableNumber-1].menu, &menuP);
			printAcount(tables[tableNumber-1].price, sail, point, tables[tableNumber-1].currentPeople);
		}

		inputCommand(command);
		if(strcmp(command,"") == 0)
		{
			continue;
		}

		detatilCommand.command = strtok(command, " ");

		if(strcmp(detatilCommand.command,"b") == 0)
		{
			if(point > 0)
			{
				sprintf(query, "select * from customer where phone='%s'",phone);
				connectDB();
				mysql_query(conn_ptr, query);
				res = mysql_store_result(conn_ptr);
				row = mysql_fetch_row(res);
				closeDB();
				checkPoint = atoi(row[3]);
				sprintf(query, "update customer set  mileage ='%d' where phone = '%s'", checkPoint + point, phone);
				connectDB();
				mysql_query(conn_ptr, query);
				closeDB();
			}

			break;
		}
		else if(strcmp(detatilCommand.command,"c") == 0)
		{
			//결제하기
			gotoxy(1,44);
			printf("                                                                                                                           ");//55
			gotoxy(1,44);
			//헌팅이 설정되어있다면.
			printf("  결제하시겠습니까? (y/n) >> ");//55			
			gets(command);


			while(1)
			{
				if(strcmp(command,"y") == 0 ||
					strcmp(command,"Y") == 0)
				{
					//결제를 한다.
					if(strcmp(phone,"")==0)
					{
						//결제하기
						gotoxy(1,44);
						printf("                                                                                                                           ");//55
						gotoxy(1,44);
						//헌팅이 설정되어있다면.
						printf("  적립하시겠습니까? (y/n) >> ");//55			
						gets(command);
						//////
						while(1)
						{
							if(strcmp(command,"y") == 0 ||
								strcmp(command,"Y") == 0)
							{

								//결제하기
								gotoxy(1,44);
								printf("                                                                                                                           ");//55
								gotoxy(1,44);
								//헌팅이 설정되어있다면.
								printf("  적립할 전화번호를 입력하세요 >> ");//55			
								gets(command);

								sprintf(query, "select * from customer where phone='%s'",command);
								strcpy(phone, command);
								connectDB();
								mysql_query(conn_ptr, query);
								res = mysql_store_result(conn_ptr);
								ok = mysql_num_rows(res);
								row = mysql_fetch_row(res);
								if(strcmp(row[3], "0")==0)
								{
									checkPoint = 0;
								}
								else
								{
									checkPoint = atoi(row[3]);
								}
								mysql_free_result(res);
								closeDB();

								sprintf(query, "update customer set mileage ='%d' where phone = '%s'",((int)(tables[tableNumber-1].price - sail - point) * 0.05) ,phone);
								connectDB();
								mysql_query(conn_ptr, query);
								closeDB();
								break;

							}	
							else if(strcmp(command,"n") == 0 ||
								strcmp(command,"N") == 0)
							{
								break;

							}
							else
							{
								errorPrint("");			
							}
						}
						/////

					}
					else
					{
						gotoxy(1,44);
						printf("                                                                                                                           ");//55
						gotoxy(1,44);
						//헌팅이 설정되어있다면.
						printf("  적립하시겠습니까? (y/n) >> ");//55			
						gets(command);
						//////
						while(1)
						{
							if(strcmp(command,"y") == 0 ||
								strcmp(command,"Y") == 0)
							{



								sprintf(query, "select * from customer where phone='%s'",phone);

								connectDB();
								mysql_query(conn_ptr, query);
								res = mysql_store_result(conn_ptr);
								row = mysql_fetch_row(res);
								if(strcmp(row[3], "0")==0)
								{
									checkPoint = 0;
								}
								else
								{
									checkPoint = atoi(row[3]);
								}
								mysql_free_result(res);
								closeDB();

								sprintf(query, "update customer set mileage ='%d' where phone = '%s'",( (int)(tables[tableNumber-1].price - sail - point) * 0.05) ,phone);
								connectDB();
								mysql_query(conn_ptr, query);
								closeDB();
								break;

							}	
							else if(strcmp(command,"n") == 0 ||
								strcmp(command,"N") == 0)
							{
								break;

							}
							else
							{
								errorPrint("");			
							}
						}
					}
					sprintf(query, "insert into amount (year, month, day, price) value (1990, 12, 4, %d) ",tables[tableNumber-1].price - sail - point);
					connectDB();
					mysql_query(conn_ptr, query);
					gotoxy(1,44);
					printf("                                                                                                                           ");//55
					gotoxy(1,44);
					//헌팅이 설정되어있다면.
					printf("  결제가 완료되었습니다. ");//55	
					getch();
					//
					//	tables[i].index = atoi(row[0]) ;
					//	strcpy(tables[i].option, row[1]);
					//	tables[i].maxPeople = atoi(row[2]);
					tables[tableNumber-1].price = 0;
					tables[tableNumber-1].currentPeople = 0;
					tables[tableNumber-1].isHaunting = FALSE;
					deleteAllMenu(&tables[tableNumber-1].menu);
					tables[tableNumber-1].menu = NULL;
					return ;

				}	
				else if(strcmp(command,"n") == 0 ||
					strcmp(command,"N") == 0)
				{
					gotoxy(1,44);
					printf("                                                                                                                           ");//55
					gotoxy(1,44);
					//헌팅이 설정되어있다면.
					printf("  결제가 취소되었습니다. ");//55	
					getch();
					break;
				}
				else
				{
					errorPrint("");			
				}
			}
		}
		else if(strcmp(detatilCommand.command,"s") == 0)
		{
			int percent;

			detatilCommand.parameter1 = strtok(NULL,"");
			if(detatilCommand.parameter1 == NULL)
			{
				errorPrint("");
				continue;
			}
			percent = atoi(detatilCommand.parameter1);
			sail = tables[tableNumber-1].price *percent /100;
			//할인적용 함수
		}
		else if(strcmp(detatilCommand.command,"p") == 0)
		{
			detatilCommand.parameter1 = strtok(NULL,"");
			if(detatilCommand.parameter1 == NULL)
			{
				errorPrint("");
				continue;
			}

			while(1)
			{


				sprintf(query, "select * from customer where phone='%s'",detatilCommand.parameter1);
				strcpy(phone, detatilCommand.parameter1);
				connectDB();
				mysql_query(conn_ptr, query);
				res = mysql_store_result(conn_ptr);
				number = mysql_num_rows(res);
				closeDB();
				if(number)
				{
					row = mysql_fetch_row(res);
					gotoxy(20, 38);
					printf("%s님의 포인트 : %s",row[0],row[3]);
					checkPoint = atoi(row[3]);

				}
				else
				{
					gotoxy(20, 38);
					printf("존재하지 않는 아이디입니다.");
					break;
				}

				gotoxy(1,44);
				printf("                                                                                                                           ");//55
				gotoxy(1,44);
				//헌팅이 설정되어있다면.

				printf("  포인트를 얼마나 사용하시겠습니까?  >> ");//55			
				gets(command);

				point = atoi(command);

				//실패
				if(point > checkPoint)
				{
					errorPrint("(포인트 부족)");
					point = 0;
				}
				else
				{
					if(tables[tableNumber-1].price < point)
					{
						errorPrint("(결제하실 금액보다 포인트가 많습니다.)");
						point = 0;
					}
					else
					{
						sprintf(query, "update customer set  mileage ='%d' where phone = '%s'", checkPoint - point, phone);
						connectDB();
						mysql_query(conn_ptr, query);
						closeDB();
					}
				}

				break;
			}
		}

		else if(!checkTotalMenu(command) )
		{
			errorPrint("");
		}
		else if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void settingTable(char *command, int tableNumber) 
{
	int peopleNumber;
	char age[AGE_LENGTH];


	settingTablePrint();
	inputCommand(command);
	strcpy(age, command);
	gotoxy(30,16);
	printf("%s", command);
	inputCommand(command);
	gotoxy(30,26);
	printf("%s", command);
	peopleNumber = atoi(command);

	gotoxy(10,34);
	printf("테이블을 지정 하시겠습니까? (y/n)  ");

	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{

			gotoxy(10,36);
			if(tables[tableNumber-1].maxPeople < peopleNumber)
			{
				printf("테이블을 지정할수 없습니다. (인원수 초과)");
			}
			else
			{
				tables[tableNumber-1].currentPeople = peopleNumber;
				strcpy(tables[tableNumber-1].age , age);
				printf("테이블을 지정하였습니다.");
			}

			getch();

			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("테이블 지정을 취소하였습니다.");
			getch();
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}

void order(char *command, int tableNumber)
{
	int amount;
	menuType *selectMenu;
	commandType orderMenu;

	while(1)
	{
		showMenu(menus, ORDER);
		inputCommand(command);
		if(strcmp(command,"") == 0)
		{
			continue;
		}
		orderMenu.command = strtok(command, " ");

		if(strcmp(orderMenu.command,"b") == 0)
		{
			break;
		}

		orderMenu.parameter1 = strtok(NULL,"");
		if(orderMenu.parameter1 == NULL)
		{
			errorPrint("");
			continue;
		}
		amount = atoi(orderMenu.parameter1);
		selectMenu = searchMenu(menus,orderMenu.command);
		if(selectMenu == NULL)
		{
			errorPrint("");
			continue;
		}
		else
		{
			selectMenu->number = amount;
			tables[tableNumber-1].price += amount * selectMenu->price;
			insertMenu(&tables[tableNumber-1].menu, selectMenu);
			break;
		}
	}
}

void cancle(char *command, int tableNumber)
{
	int amount;
	menuType *selectMenu;
	commandType cancleMenu;

	while(1)
	{
		showMenu(tables[tableNumber-1].menu, CANCLE);
		inputCommand(command);
		if(strcmp(command,"") == 0)
		{
			continue;
		}
		cancleMenu.command = strtok(command, " ");

		if(strcmp(cancleMenu.command,"b") == 0)
		{
			break;
		}

		cancleMenu.parameter1 = strtok(NULL,"");
		if(cancleMenu.parameter1 == NULL)
		{
			errorPrint("");
			continue;
		}
		amount = atoi(cancleMenu.parameter1);
		selectMenu = searchMenu(tables[tableNumber-1].menu,cancleMenu.command);

		if(selectMenu == NULL)
		{
			errorPrint("");
			continue;
		}
		else
		{
			if(selectMenu->number < amount)
			{
				errorPrint("");
				continue;
			}
			else if(selectMenu->number == amount)
			{
				//삭제
				tables[tableNumber-1].price -= amount * selectMenu->price;
				deleteMenu(&tables[tableNumber-1].menu, selectMenu->code);
			}
			else
			{
				selectMenu->number -= amount;
				tables[tableNumber-1].price -= amount * selectMenu->price;
			}
			break;
		}
	}
}

void haunting(char *command, int tableNumber)
{
	if(tableNumber<1 || tableNumber > totalTableNumber)
	{
		errorPrint("(테이블이 존재하지 않습니다.)");
		return ;
	}
	else if(tables[tableNumber-1].currentPeople == 0)
	{
		errorPrint("(테이블이 비어있습니다.)");	
		return ;

	}
	while(1)
	{
		gotoxy(1,44);
		printf("                                                                                                                           ");//55
		gotoxy(1,44);
		//헌팅이 설정되어있다면.

		if(tables[tableNumber-1].isHaunting)
		{
			printf("  \"%d\" 테이블의 헌팅을 해제하시겠습니까? (y/n) >> ",tableNumber);//55			
		}
		else
		{
			printf("  \"%d\" 테이블의 헌팅을 설정하시겠습니까? (y/n) >> ",tableNumber);//55
		}
		gets(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			if(tables[tableNumber-1].isHaunting)
			{
				tables[tableNumber-1].isHaunting = 0;
			}
			else
			{
				tables[tableNumber-1].isHaunting = 1;
			}
			break;
		}
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}
void tableAdd(char *command, int to, int from)
{
	while(1)
	{
		gotoxy(1,44);
		printf("                                                                                                                           ");//55
		gotoxy(1,44);
		//헌팅이 설정되어있다면.
		if(to<1 || to > totalTableNumber || from<1 || from > totalTableNumber)
		{
			errorPrint("(테이블이 존재하지 않습니다.)");
			break;
		}
		if(tables[from-1].currentPeople == 0)
		{
			errorPrint("(테이블이 비어있습니다.)");
			break;
		}
		else if(tables[to-1].currentPeople == 0)
		{
			printf("  \"%d\"테이블에서 \"%d\" 테이블로 이동하시겠습니까? (y/n) >> ", from, to);
		}
		else
		{
			printf("  \"%d\" 과 \"%d\" 테이블을 합치시겠습니까? (y/n) >> ", from, to);
		}
		gets(command);

		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			//테이블 합치는작업실행

			if(tables[to-1].currentPeople + tables[from-1].currentPeople > tables[to-1].maxPeople)
			{
				errorPrint("테이블 한도를 초과하였습니다.");
			}
			else
			{
				moveMenu(tables[from-1].menu, &tables[to-1].menu);
				deleteAllMenu(&tables[from-1].menu);
				tables[to-1].currentPeople += tables[from-1].currentPeople;
				tables[from-1].currentPeople = 0;
				tables[to-1].price += tables[from-1].price;
				
				tables[from-1].isHaunting = FALSE;
				tables[from-1].price = 0;
			}
			break;
		}
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}

void reservation(char *command)
{
	while(1)
	{
		reservationPrint();
		inputCommand(command);
		if(strcmp(command,"b") ==0)
		{
			break;
		}
		else if(strcmp(command,"1") ==0)
		{
			reservationSelectMode(command);
			//예약자확인
		}
		else if(strcmp(command,"2") ==0)
		{
			//예약하기
			addReservation(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void reservationSelectMode(char *command)
{
	while(1)
	{
		reservationSelectModePrint();
		inputCommand(command);
		if(strcmp(command,"b") ==0)
		{
			break;
		}
		else if(strcmp(command,"1") ==0)
		{
			//예약자확인
			reservationAllPrint(command);
		}
		else if(strcmp(command,"2") ==0)
		{
			//예약자검색
			reservationCheck(command);
		}
		else if(!checkTotalMenu(command))
		{
			errorPrint("");
		}

		if(checkTotalMenu(command))
		{
			break;
		}
	}
}

void reservationCheck(char *command)
{
	MYSQL_RES* res;
	MYSQL_ROW row;
	int number;
	char search[SEARCH_LENGTH];
	char query[QUERY_LENGTH];

	reservationCheckPrint();
	inputCommand(command);
	sprintf(query, "select * from reservation where phone='%s'",command);


	strcpy(search, command);
	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	number = mysql_num_rows(res);
	if(number)
	{
		while(1)
		{
			resuervationResultPrint();
			gotoxy(10,12);
			printf("▶ '%s'의 예약 결과입니다.",search);
			reservationInformation(res);
			mysql_free_result(res);
			closeDB();
			connectDB();
			mysql_query(conn_ptr, query);
			res = mysql_store_result(conn_ptr);

			inputCommand(command);
			if(strcmp(command,"b") ==0)
			{
				break;
			}
			else
			{
				errorPrint("(뒤로가기를 선택하여 주시기 바랍니다.)");
				continue;
			}
		}
	}
	else
	{
		noMemberPrint();
		getch();
	}
	closeDB();
}	



void reservationAllPrint(char *command)
{
	char query[QUERY_LENGTH];
	int ok;
	int number;

	int currentPage = 1;
	int maxPage;
	MYSQL_RES* res;
	MYSQL_ROW row;

	sprintf(query, "select * from reservation");

	connectDB();
	mysql_query(conn_ptr, query);
	res = mysql_store_result(conn_ptr);
	number = mysql_num_rows(res);
	maxPage = number % 20 == 0 ? number/20 : (number/20) + 1 ;

	if(number)
	{
		while(1)
		{
			resuervationResultPrint();
			gotoxy(1,38);
			printf("│                                                                            이전페이지[p] / 다음페이지 [n] (%d/%d)      │\n",currentPage, maxPage);//3
			printInformation(res, currentPage);
			mysql_free_result(res);
			closeDB();
			connectDB();
			mysql_query(conn_ptr, query);
			res = mysql_store_result(conn_ptr);

			inputCommand(command);
			if(strcmp(command,"b") ==0)
			{
				break;
			}
			else if(strcmp(command,"n") == 0)
			{
				//다음페이지
				if( currentPage < maxPage)
				{
					currentPage++;
				}	
			}
			else if(strcmp(command,"p") == 0)
			{
				//이전페이지
				if( currentPage > 1)
				{
					currentPage--;
				}	
			}
			else
			{
				errorPrint("");
				continue;

			}
		}
	}
	else
	{
		noMemberPrint();
		getch();

	}
	closeDB();
}


void printreservation(MYSQL_RES* res, int currentPage)
{
	int i;
	int y=16;
	MYSQL_ROW row;

	for(i = 0 ; i < (currentPage-1)*20 ; i++)
	{
		mysql_fetch_row(res);
	}
	i=0;
	while((row = mysql_fetch_row(res)) != NULL && i < 20)
	{
		gotoxy(12,y);
		printf("%s",row[0]);//10
		gotoxy(26,y);
		printf("%s",row[1]);//10
		gotoxy(42,y);
		printf("%s",row[2]);//10
		gotoxy(63,y);
		printf("%s",row[3]);//10
		gotoxy(83,y++);
		printf("%s",row[3]);//10
		i++;
	}
}

void addReservation(char *command)
{
	char query[QUERY_LENGTH];

	addReservationPrint();
	sprintf(query, "insert into reservation value ('");

	inputCommand(command);
	strcat(query, command);
	strcat(query, "', '");
	gotoxy(26,16);
	printf("%s",command);

	inputCommand(command);
	strcat(query, command);
	strcat(query, "', '");
	gotoxy(26,20);
	printf("%s",command);

	inputCommand(command);
	strcat(query, command);
	strcat(query, "', '");
	gotoxy(26,24);
	printf("%s",command);

	inputCommand(command);
	strcat(query, command);
	strcat(query, "', '");
	gotoxy(26,28);
	printf("%s",command);

	inputCommand(command);
	strcat(query, command);
	strcat(query, "')");
	gotoxy(26,32);
	printf("%s",command);


	gotoxy(10,34);
	printf("예약하시겠습니까? (y/n)  ");

	while(1)
	{
		inputCommand(command);
		if(strcmp(command,"y") == 0 ||
			strcmp(command,"Y") == 0)
		{
			int ok;
			gotoxy(10,36);

			connectDB();
			ok = mysql_query(conn_ptr, query);
			closeDB();
			if(!ok)
			{
				printf("예약이 완료되었습니다.");
				getch();
			}
			else
			{
				printf("예약할 수 없습니다. (동일한 핸드폰으로 예약완료)");
				getch();
			}
			break;
		}	
		else if(strcmp(command,"n") == 0 ||
			strcmp(command,"N") == 0)
		{
			gotoxy(10,36);
			printf("예약을 취소하였습니다.");
			getch();
			break;
		}
		else
		{
			errorPrint("");			
		}
	}
}

void reservationInformation(MYSQL_RES* res)
{
	int y=16;
	MYSQL_ROW row;

	while((row = mysql_fetch_row(res)) != NULL)
	{
		gotoxy(12,y);
		printf("%s",row[0]);//10
		gotoxy(26,y);
		printf("%s",row[1]);//10
		gotoxy(42,y);
		printf("%s",row[2]);//10
		gotoxy(63,y);
		printf("%s",row[3]);//10
		gotoxy(83,y++);
		printf("%s",row[3]);//10
	}
}


void tableInit()
{
	char query[QUERY_LENGTH];
	int i=0;
	MYSQL_RES* res;
	MYSQL_ROW row;

	free(tables);
	connectDB();
	mysql_query(conn_ptr,"select * from tables");
	res = mysql_store_result(conn_ptr);
	totalTableNumber = mysql_num_rows(res);

	tables = (tableType*)malloc(sizeof(tableType)*totalTableNumber);

	while((row = mysql_fetch_row(res)) != NULL)
	{
		tables[i].index = atoi(row[0]) ;
		strcpy(tables[i].option, row[1]);
		tables[i].maxPeople = atoi(row[2]);
		tables[i].price = 0;
		tables[i].currentPeople = 0;
		tables[i].isHaunting = FALSE;
		tables[i].menu = NULL;
		i++;
	}
	closeDB();



}
void menuInit()
{
	char query[QUERY_LENGTH];
	menuType menu;
	MYSQL_RES* res;
	MYSQL_ROW row;
	deleteAllMenu(&menus);


	connectDB();
	mysql_query(conn_ptr,"select * from menu");
	res = mysql_store_result(conn_ptr);

	while((row = mysql_fetch_row(res)) != NULL)
	{
		strcpy(menu.code, row[3]);
		strcpy(menu.group, row[2]);
		strcpy(menu.name, row[0]);
		menu.price = atoi(row[1]);
		menu.number = 0;

		insertMenu(&menus, &menu);
	}
	closeDB();

}