#ifndef STATE_H
#define STATE_H

void state(char *command);
void order(char *command, int tableNumber);
void cancle(char *command);
void haunting(char *command);
void tableAdd(char *command);
void reservation(char *command);
void nextPage();
void prevPage();
void settingTable(char *command, int tableNumber);
void tableInit();
void menuInit();
void reservationInformation(MYSQL_RES* res);
void reservationCheck(char *command);
void addReservation(char *command);
void reservationSelectMode(char *command);
void reservationAllPrint(char *command);
void printreservation(MYSQL_RES* res, int currentPage);
void detailAcount(char *command, int tableNumber);
#endif