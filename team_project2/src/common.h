#ifndef COMMON_H
#define COMMON_H

#define PASSWORD_LENGTH 50
#define COMMAND_LENGTH 100
#define MENU_NAME_LENGTH 100
#define MENU_GROUP_LENGTH 20
#define USER_NAME_LENGTH 100
#define PHONE_LENGTH 20
#define BIRTHDAY_LENGTH 10
#define CODE_LENGTH 10
#define AGE_LENGTH 10
#define QUERY_LENGTH 200
#define SEARCH_LENGTH 100

#define HOST "192.168.0.19"
#define USER "root"
#define PASS "root"
#define NAME "hp"

#define OPTION_LENGTH 100
#define TRUE 1
#define FALSE 0

#define ORDER 1
#define CANCLE 0


#include <stdio.h>
#include "gotoxy.h"
#include <string.h>

//디비관련

#include <my_global.h>
#include <errno.h>
#include <mysql.h>
#pragma comment(lib, "libmysql.lib") 
#pragma comment(lib, "ws2_32.lib")
//#pragma comment(lib, "C:\\Users\\한종빈\\Downloads\\mysql-5.5.28-winx64\\mysql-5.5.28-winx64\\libmysql.lib") 


//
typedef struct menu
{
	char code[CODE_LENGTH];
	char name[MENU_NAME_LENGTH];
	char group[MENU_GROUP_LENGTH];
	int price;
	int number;
}menuType;

typedef struct menuPType
{
	int x, y;
	int sidedishCount;
	int drinkCount;
	int beverageCount;
}menuPType;


typedef struct member
{
	char name[USER_NAME_LENGTH];
	char phone[PHONE_LENGTH];
	char birthDay[BIRTHDAY_LENGTH];
	int mileage;
}member;

typedef struct menuTree
{
	menuType menu;
	struct menuTree *left, *right;
}menuTree;




typedef struct commandType
{
	char *command;
	char *parameter1;
	char *parameter2;
}commandType;

typedef struct dateType
{
	char year[5];
	char month[3];
	char day[3];
}dateType;
typedef struct tableType
{
	int index;
	int maxPeople;
	int currentPeople;
	int price;
	int isHaunting;
	char option[OPTION_LENGTH];
	char age[AGE_LENGTH];
	menuTree *menu;
}tableType;

extern MYSQL *conn_ptr;

void customerMain();
void setting();
int login(char *password);
int mainPrint();
void inputCommand(char *command);
void joinMemberPrint();
void memberInformationPrint();
void errorPrint(char *msg);
void selectSearchMemberPrint();
int checkTotalMenu(char *command);
void nameSearchPrint();
int phoneSearchPrint();
void updateMemberPrint();
void dropMemberPrint();
void searchResultPrint();
int totalMemberPrint();
void managerMain();
void tableSetupPrint();
void acountSetupPrint();
void initializationPrint();
void stateMain(tableType *tables, int currentPage, int tableNumber);
void createTablePrint();
void deleteTablePrint();
void updateTablePrint();
void reservationPrint();
void settingTablePrint();
void connectDB();
void closeDB();
void changeInformationPrint();
void noMemberPrint();
void notablePrint();
void tableInformationPrint(MYSQL_RES* res, int currentPage);
void noMenuPrint();
void menuInformationPrint();
void detailMenuPrint(MYSQL_RES* res, menuPType *menuP );
void addMenuPrint();
void updateMenuPrint();
void deleteMenuPrint();
void resetPrint(char *title);
void amountPrint(char *title);
int reservationCheckPrint();
void resuervationResultPrint();
void addReservationPrint();
void reservationSelectModePrint() ;
void menuDetailInformationPrint();

#endif 