
#include "menu.h"


//책 제목으로 책 추가함수
void insertMenu(menuTree **root, menuType *menu) 
{
	menuTree *p, *t; 
	menuTree *n;	 
	t = *root;
	p = NULL;

	while (t != NULL){
		if( strcmp( menu->code , t->menu.code) == 0 )
		{
			//view_message("책이 이미 존재 합니다.\n");
			t->menu.number += menu->number;
			return;
		}
		p = t;
		if( strcmp( menu->code , t->menu.code) < 0 )
		{
			t = t->left;
		}
		else
		{
			t = t->right;
		}
	}
	n = (menuTree *) malloc(sizeof(menuTree));
	if( n == NULL )
	{
		return;
	}
	//메뉴추가
	strcpy(n->menu.name , menu->name);
	strcpy(n->menu.code , menu->code);
	n->menu.price = menu->price ;
	strcpy(n->menu.group , menu->group);
	n->menu.number =  menu->number;
	n->left = n->right = NULL;

	if( p != NULL ) 
		if( strcmp( menu->code , p->menu.code) < 0) 
			p->left = n;
		else p->right = n;
	else *root = n;
}


menuType*  searchMenu(menuTree  *root,  char *code)
{
	if ( root == NULL ) 
	{
		return NULL;
	}
	if ( strcmp(code, root->menu.code) ==0 )
	{
		return &root->menu;
	}
	else if ( strcmp(code, root->menu.code) < 0 )
	{
		return searchMenu(root->left, code);     
	}
	else
	{
		return searchMenu(root->right, code); 
	}
}

//모든 고객 정보를 보여주는함수
void showMenu(menuTree *root, int mode)
{
	menuPType menuP= {0,};
	gotoxy(1,6);
	printf("│                                                                                                                      │\n");//3
	printf("│  ┌─────────────────────────┐      ┌─────────────────────────┐  │\n");//6
	printf("│  │  ┌─────────┐                          │      │  ┌─────────┐                          │  │\n");//7
	printf("│  │  │                  │                          │      │  │                  │                          │  │\n");//6
	printf("│  │  │    안       주   │                          │      │  │    음       료   │                          │  │\n");//7
	printf("│  │  │                  │                          │      │  │                  │                          │  │\n");//8
	printf("│  │  └─────────┘                          │      │  └─────────┘                          │  │\n");//9
	printf("│  │                                                  │      │                                                  │  │\n");//70
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//10
	printf("│  │                                                  │      │                                                  │  │\n");//1
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │   ┌─────────┐                         │  │\n");//7
	printf("│  │                                                  │      │   │                  │                         │  │\n");//70
	printf("│  │                                                  │      │   │   주        류   │                         │  │\n");//7
	printf("│  │                                                  │      │   │                  │                         │  │\n");//7
	printf("│  │                                                  │      │   └─────────┘                         │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//10
	printf("│  │                                                  │      │                                                  │  │\n");//10
	printf("│  │                                                  │      │                                                  │  │\n");//1
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//70
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  │                                                  │      │                                                  │  │\n");//7
	printf("│  └─────────────────────────┘      └─────────────────────────┘  │\n");//7
	printf("│                                                                                                                      │\n");//10

	printMenu(root, &menuP, mode);
}

void printMenu(menuTree *root, menuPType *menuP,int mode)
{
	if(root)
	{
		if(strcmp(root->menu.group, "안주") == 0)
		{
			menuP->x = 10;
			menuP->y = 14 + menuP->sidedishCount++;
		}
		else if(strcmp(root->menu.group, "음료") == 0)
		{
			menuP->x = 70;
			menuP->y = 14 + menuP->beverageCount++;
		}
		else
		{
			menuP->x = 70;
			menuP->y = 28 + menuP->drinkCount++;
		}
		if(mode)
		{
			gotoxy(menuP->x, menuP->y);
			printf("%3s\t", root->menu.code);
			printf("%20s\t", root->menu.name);
			printf("%d", root->menu.price);
		}
		else 
		{
			gotoxy(menuP->x, menuP->y);
			printf("%3s\t", root->menu.code);
			printf("%20s\t", root->menu.name);
			printf("%d", root->menu.number);
		}
		printMenu(root->left, menuP, mode);
		printMenu(root->right, menuP, mode);
	}
}

void deleteMenu(menuTree **root, char *code)
{
	menuTree *p, *child, *succ, *succ_p, *t;
	p = NULL;
	t = *root;
	while( t != NULL && strcmp(code, t->menu.code) != 0 )
	{
		p = t;
		t = ( strcmp(code, t->menu.code) < 0 ) ? t->left : t->right;
	}
	if( t == NULL )
	{ 
		return;
	}

	if( (t->left==NULL) && (t->right==NULL) )
	{ 
		if( p != NULL )
		{
			if( p->left == t )	
			{
				p->left = NULL;
			}
			else
			{
				p->right = NULL;
			}
		}
		else
		{
			*root = NULL;
		}
	}

	else if((t->left==NULL)||(t->right==NULL))
	{
		child = (t->left != NULL) ? t->left : t->right;
		if( p != NULL )
		{
			if( p->left == t )
			{ 
				p->left = child;
			}
			else
			{
				p->right = child;
			}
		}
		else 
		{	
			*root = child;
		}
	}
	else
	{		
		succ_p = t;
		succ = t->right;
		while(succ->left != NULL)
		{
			succ_p = succ;
			succ = succ->left;
		}
		if( succ_p->left == succ )
		{
			succ_p->left = succ->right;
		}
		else 
		{
			succ_p->right = succ->right;
		}
		t->menu = succ->menu;
		t = succ;
	}
	free(t);
}


void moveMenu(menuTree *from, menuTree **to)
{
	if(from)
	{
		moveMenu(from->left,to);
		moveMenu(from->right, to);
		insertMenu(to, &from->menu) ;
	}
}

void deleteAllMenu(menuTree **root)
{
	if( (*root) )
	{
		
		deleteAllMenu(&(*root)->left);
		deleteAllMenu(&(*root)->right);
		deleteMenu(root, (*root)->menu.code);
	}
}


void printdetailTableInfomation(menuTree *root, menuPType *menuP)
{
	if(root)
	{
		if(strcmp(root->menu.group, "안주") == 0)
		{
			menuP->x = 10;
			menuP->y = 17 + menuP->sidedishCount++;
		}
		else if(strcmp(root->menu.group, "음료") == 0)
		{
			menuP->x = 10;
			menuP->y = 34 + menuP->drinkCount++;
		}
		else 
		{
			menuP->x = 10;
			menuP->y = 27 + menuP->beverageCount++;
		}
		
		
		gotoxy(menuP->x, menuP->y);
		printf("%-20s\t", root->menu.name);
		gotoxy(menuP->x+30, menuP->y);
		printf("%d", root->menu.price);
		gotoxy(menuP->x+40, menuP->y);
		printf("%d", root->menu.number);
		printdetailTableInfomation(root->left, menuP);
		printdetailTableInfomation(root->right, menuP);
	}
}


void printAcount(int price, int sail, int point, int peopleNumber)
{
	int result = price-point-sail;
	gotoxy(83,15);
	printf("%d",price);
	gotoxy(83,20);
	printf("%d",sail);
	gotoxy(83,23);
	printf("%d",point);	
	gotoxy(83,29);
	printf("%d",result);
	gotoxy(83,35);
	printf("%d",result / peopleNumber);
}