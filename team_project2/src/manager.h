#ifndef MANAGER_H
#define MANAGER_H

void manager(char *command);
void tableSetup(char *command);
void menuSetup(char *command);
void acountSetup(char *command);
void initialization(char *command);
void createTable(char *command);
void deleteTable(char *command);
void updateTable(char *command);
void tableInformation(char *command);
void menuInformation(char *command);
void addMenu(char *command);
void updateMenu(char *command);
void dropMenu(char *command);
void tableReset(char *command);
void customerReset(char *command);
void menuReset(char *command);
void allReset(char *command);
void dayAmount(char *command);
void monthAmount(char *command);
void yearAmount(char *command);
#endif