#ifndef CUSTOMER_H
#define CUSTOMER_H

void customer(char *command);
void joinMember();
void information(char *command);
void search(char *command);
void drop(char *command);
void update(char *command);
void totalMember(char *command);

#endif