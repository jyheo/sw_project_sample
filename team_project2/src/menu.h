#ifndef MENU_H
#define MENU_H

#include "common.h"

void showMenu(menuTree *root, int mode);
void insertMenu(menuTree **root, menuType *menu);
void printMenu(menuTree *root, menuPType *menuP, int mode);
menuType  *searchMenu(menuTree  *root,  char *code);
void deleteMenu(menuTree **root, char *code);
void moveMenu(menuTree *from, menuTree **to);
void deleteAllMenu(menuTree **root);
void printdetailTableInfomation(menuTree *root, menuPType *menuP);
void printAcount(int price, int sail, int point, int peopleNumber);

#endif