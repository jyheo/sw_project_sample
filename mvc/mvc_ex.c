#include "stdafx.h"

#define MAX_BOOKS	100

struct my_model {
	int idx;
	char title[200];
	int checkout; // 1: checkout, 0:checkin
} book_model[MAX_BOOKS] = {
	{1, "이기적 유전자", 0},
	{2, "자료구조", 0},
	{3, "운영체제", 0},
	{4, "분산시스템", 0},
	{5, "1Q84", 0},
	{6, "일각수의 꿈", 0},
	{7, "삼국지", 0},
	{8, "흐르는 강물처럼", 0},
	{9, "논리적 글쓰기", 0},
	{10, "그리스 로마 신화", 0},
	{11, "로마인 이야기", 0}
};

int total_books;

int model_init()
{
	int i;
	for (i = 0; i < MAX_BOOKS; i++) {
		if (book_model[i].idx <= 0)
			break;
	}
	total_books = i;
	return total_books;
}

int model_get_num_of_books()
{
	return total_books;
}

struct my_model *model_get_books(int idx)
{
	int i;
	for (i = 0; i < MAX_BOOKS; i++) {
		if (book_model[i].idx == idx)
			return &book_model[i];
	}
	return NULL;
}

int model_set_checkout(int idx, int checkout)
{
	int i;
	if (checkout != 0 && checkout != 1)
		return 0;
	for (i = 0; i < MAX_BOOKS; i++) {
		if (book_model[i].idx == idx) {
			book_model[i].checkout = checkout;
			return 1;
		}
	}
	return 0;
}



int view_init()
{
	printf("도서관리 시스템: 도서수:%d\n", model_get_num_of_books());
	return 0;
}

int view_top_menu()
{
	int input;

	do {
		printf("==상위 메뉴==\n");
		printf("[1] 조회\n[2] 대출\n[3] 반납\n");
		printf("입력:\n");
		scanf("%d", &input);
		if (input == 1 || input == 2 || input == 3)
			return input;
	} while(1);

	return 0;
}

int view_checkout_menu()
{
	int input;

	do {
		printf("==대출 메뉴==\n");
		printf("도서번호입력:\n");
		scanf("%d", &input);
		if (input > 0)
			return input;
	} while(1);
	return 0;
}

int view_checkin_menu()
{
	int input;

	do {
		printf("==반납 메뉴==\n");
		printf("도서번호입력:\n");
		scanf("%d", &input);
		if (input > 0)
			return input;
	} while(1);

	return 0;
}

int view_info_menu()
{
	int input;

	do {
		printf("==조회 메뉴==\n");
		printf("도서번호입력:\n");
		scanf("%d", &input);
		if (input > 0)
			return input;
	} while(1);

	return 0;
}




int view_book_info(int idx)
{
	struct my_model *pmodel;
	pmodel = model_get_books(idx);

	if (pmodel) {
		printf("도서 조회 결과\n");
		printf("%d %s %s\n", pmodel->idx, pmodel->title, pmodel->checkout ? "대출중" : "대출가능");
		return 1;
	} else {
		return 0;
	}
}

int view_message(char *msg)
{
	printf("메시지: %s\n", msg);
	return 1;
}




int con_checkout(int idx)
{
	if (model_set_checkout(idx, 1)) {
		view_message("대출하였습니다.\n");
		return 1;
	}
	return 0;
}

int con_checkin(int idx)
{
	if (model_set_checkout(idx, 0)) {
		view_message("반납하였습니다.\n");
		return 1;
	}
	return 0;
}

int con_info(int idx)
{
	if (idx > 0)
		return view_book_info(idx);
	else
		return 0;
}





int _tmain(int argc, _TCHAR* argv[])
{
	int input;
	model_init();
	view_init();

	do {
		input = view_top_menu();
		switch(input) {
		case 1:
			input = view_info_menu();
			con_info(input);
			break;
		case 2:
			input = view_checkout_menu();
			con_checkout(input);
			break;
		case 3:
			input = view_checkin_menu();
			con_checkin(input);
			break;
		}
	} while(1);

	return 0;
}

