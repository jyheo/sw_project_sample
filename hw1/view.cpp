#include "book.h"
#include "model.h"
#include "controller.h"
#include <stdio.h>
#include <stdlib.h>


//프로그램 시작시 보여주는 함수
int view_()
{
	int select;
	do {
		fflush(stdin);
		system("cls");
		printf("==도서관리 프로그램==\n");
		printf("┌────────────────────────┐\n");
		printf("│* 본프로그램은 도서관리 프로그램입니다.         │\n");
		printf("│────────────────────────│\n");
		printf("│ -로그인 후 책대여를 하실수 있습니다.           │\n");
		printf("│ -도서관리 및 회원관리는 관리자만 할수있습니다. │\n");
		printf("│ -기타 궁금한점이있으면 관리자에게 연락바랍니다.│\n");
		printf("│   ( E-mail : stompesi@gmail.com )              │\n");
		printf("└────────────────────────┘\n");
		printf("[1] 로그인\n[2] 회원가입\n[3] 아이디 찾기\n[4] 비밀번호 찾기\n[5] 관리자 모드\n[6] 종료\n");
		printf("입력: ");
		fflush(stdin);
		scanf("%d", &select);
		if (select == 1 || select == 2 || select == 3 || select == 4 || select == 5 || select == 6)
			return select;
	} while(1);
	return 0;
}

//회원가입 방법보여주는 함수 
personModel view_joinForm()
{
	personModel newPerson;
	int select;

	fflush(stdin);
	system("cls");
	printf("== 회원 가입 ==\n");
	printf("[1]  아이디 : ");
	gets(newPerson.id);
	printf("[2] 비밀번호: ");
	gets(newPerson.password);
	printf("[3]  이 름  : ");
	gets(newPerson.name);
	printf("[3]  e-mail : ");
	gets(newPerson.email);

	return newPerson;
}
//고객 로그인시 초기화면
int view_login_selectMenu()
{
	int select;
	do {
		fflush(stdin);
		system("cls");
		//로그인 화면 보여주기
		printf("==도서==\n");
		printf("[1] 목록 [2] 조회 [3] 대출 [4] 반납 [5] 로그아웃\n");
		printf("입력: ");
		fflush(stdin);
		scanf("%d", &select);
		if (select == 1 || select == 2 || select == 3 || select == 4 || select == 5)
			return select;
	} while(1);
	return 0;
}

//관리자 로그인시 초기 화면
int view_admin_selectMenu()
{
	int select;
	do {
		fflush(stdin);
		system("cls");
		//로그인 화면 보여주기
		printf("== 관리자 모드 ==\n");
		printf("[1] 도서관리 [2] 회원관리 [3] 로그아웃\n");
		printf("입력: ");
		fflush(stdin);
		scanf("%d", &select);
		if (select == 1 || select == 2 || select ==3)
			return select;
	} while(1);
	return 0;
}

//도서검색 방법 선택 화면
int view_book_search_selectMenu()
{
	int select;

	do {
		system("cls");
		printf("== 도서 검색 ==\n");
		printf("1. 도서번호 검색\n");
		printf("2. 도 서 명 검색\n");
		fflush(stdin);
		scanf("%d", &select);
		if (select == 1 || select ==2 || select ==3)
		{
			return select;
		}
	} while(1);

	return 0;
}



//도서 인덱스로 검사하는 함수
void view_book_searchIndex(int *idx)
{
	do {
		system("cls");
		printf("== Index 도서검색 ==\n");
		printf("도서번호 입력 : ");
		scanf("%d", idx);
		if (*idx >= 0)
		{
			return ;
		}
	} while(1);
	return ;
}

//도서 제목으로 검사하는 함수
void view_book_searchTitle(char *title)
{
	system("cls");
	printf("== Title 도서검색 ==\n");
	printf("도서명 입력 : ");
	fflush(stdin);
	gets(title);
}

//도서 정보 보여주는 함수
void view_book_showInfo(TreeNode *root)
{
	if(root)
	{
		printf("───────────────────\n");
		printf(" 번  호  : %d \n", root->book.idx);
		printf(" 제  목  : %s \n", root->book.title);
		printf(" 위  치  : %d층 \n", root->book.position);
		if(root->book.checkout==0)
		{
			printf("대출가능 : o \n", root->book.checkout);
		}
		else
		{
			printf("대출가능 : X : 대출자(%s)\n", root->book.borrower);
		}
		printf("───────────────────\n");
	}
	else
	{
		printf("───────────────────\n");
		printf("찾으시는 책이 없습니다.\n");
		printf("───────────────────\n");	
	}
	fflush(stdin);
	getchar();
}
int view_book_change_position()
{
	int position;

	do{
	printf("책 위치(1~5층만 가능) : ");
	scanf("%d",&position);
	}while(position > 5 || position < 1);
	return position;
}

//메시지 보여주는 함수
void view_message(char *msg)
{
	fflush(stdin);
	printf("%s\n", msg);
	getchar();
}

//빌리는 방법 선택하는 화면
int view_book_loan_selectMenu()
{
	int select;

	do {
		system("cls");
		printf("== 도서 대출 ==\n");
		printf("1. 도서번호 대출\n");
		printf("2. 도 서 명 대출\n");
		fflush(stdin);
		scanf("%d", &select);
		if (select == 1 || select ==2)
		{
			return select;
		}
	} while(1);

	return 0;
}


//인덱스로 책 빌리는 함수
void view_book_loanIndex(int *idx)
{
	do {
		system("cls");
		printf("== Index 대출 ==\n");
		printf("도서번호 입력 : ");
		scanf("%d", idx);
		if (*idx >= 0)
		{
			return ;
		}
	} while(1);
	return ;
}

//제목으로 책빌리는 함수
void view_book_loanTitle(char *title)
{
	system("cls");
	printf("== Title 대출 ==\n");
	printf("도서명 입력 : ");
	fflush(stdin);
	gets(title);
}

//책 반납 방법 선택함수
int view_book_return_menuSelect()
{
	int select;
	do {
		system("cls");
		printf("== 도서 반납 ==\n");
		printf("1. 도서번호 반납\n");
		printf("2. 도 서 명 반납\n");
		fflush(stdin);
		scanf("%d", &select);
		if (select == 1 || select ==2)
		{
			return select;
		}
	} while(1);

	return 0;
}

//반납할 책번호를 입력받는 함수
void view_book_returnIndex(int *idx)
{
	do {
		system("cls");
		printf("== Index 반납 ==\n");
		printf("도서번호 입력 : ");
		scanf("%d", idx);
		if (*idx >= 0)
		{
			return ;
		}
	} while(1);
	return ;
}

//반납할 책제목을 입력받는 함수
void view_book_returnTitle(char *title)
{
	system("cls");
	printf("== Title 반납 ==\n");
	printf("도서명 입력 : ");
	fflush(stdin);
	gets(title);
}


//추가할 책이름을 받는 함수
void view_book_insert(char *title)
{
	system("cls");
	printf("== 도서 추가 ==\n");
	printf("도서명 입력 : ");
	fflush(stdin);
	gets(title);
}


//삭제할 책 이름을 받는함수
void view_book_delete(char *title)
{
	printf("== 도서 삭제 ==\n");
	printf("도서명 입력 : ");
	fflush(stdin);
	gets(title);
}

//수정할 책 이름을 받는함수
void view_book_change(char *title)
{
	printf("== 도서 수정 ==\n");
	printf("도서명 입력 : ");
	fflush(stdin);
	gets(title);
}

//관리자 로그인 비밀번호 받는함수
personModel view_admin_login()
{
	personModel check;

	fflush(stdin);
	system("cls");
	printf("== 로그인 ==\n");
	printf("[1] 비밀번호: ");
	gets(check.password);

	return check;
}
//로그인 할 고객 정보를 받는함수
personModel view_customer_login()
{
	personModel customer;

	fflush(stdin);
	system("cls");
	printf("== 로그인 ==\n");
	printf("[1]  아이디 : ");
	gets(customer.id);
	printf("[2] 비밀번호: ");
	gets(customer.password);

	return customer;
}

//관리자 책관리 매뉴선택함수
int view_admin_book_selectMenu()
{
	int select;
	fflush(stdin);
	system("cls");
	printf("==도서 관리 ==\n");
	printf("[1] 목록 [2] 추가 [3] 수정(위치) [4] 삭제 [5] 뒤로\n");
	printf("입력: ");
	scanf("%d",&select);

	return select;

}

//관리자 고객 매뉴선택함수
int view_admin_customer_selectMenu()
{
	int select;
	fflush(stdin);
	system("cls");
	printf("== 고객 관리 ==\n");
	printf("[1] 목록 [2] 강제탈퇴 [3] 뒤로\n");
	printf("입력: ");
	scanf("%d",&select);

	return select;

}

//모든 고객 정보를 보여주는함수
void view_customer_showAll(customerTreeNode *root)
{

		printf("───────────────────\n");
		printf(" 아이디  : %s \n", root->customer.id);
		printf("비밀번호 : %s \n", root->customer.password);
		printf(" 이  름  : %s \n", root->customer.name);
		printf(" 이메일  : %s \n", root->customer.email);
		printf("───────────────────\n");
}

//탈퇴시킬 고객 정보를 받는함수
void view_customer_delete(char *title)
{
	fflush(stdin);
	printf("== 사용자 탈퇴 ==\n");
	printf("삭제할 아이디 입력 : ");
	fflush(stdin);
	gets(title);
}

//비밀번호 찾을때 관련 정보를 받는함수
void view_customer_findPassword(personModel *findmodel)
{
	fflush(stdin);
	system("cls");
	printf("== 비밀번호 찾기 ==\n");
	printf("아이디 입력 : ");
	gets(findmodel->id);
	printf("email 입력 : ");
	gets(findmodel->email);
}

//비밀번호 정보 보여주는 함수
void view_customper_showPassword(customerTreeNode *root)
{
	if(root)
	{
		printf("───────────────────\n");
		printf("찾으시는 비밀번호는 : ' %s ' 입니다.\n",root->customer.password);
		printf("───────────────────\n");
	}
	else
	{
		printf("비밀번호를 찾으실수 없습니다.\n");
	}
	getchar();
}
//아이디 찾을때 관련 정보를 받는함수
void view_customer_findId(personModel *findmodel)
{
	fflush(stdin);
	system("cls");
	printf("== 아이디 찾기 ==\n");
	printf("이름 입력 : ");
	gets(findmodel->name);
	printf("email 입력 : ");
	gets(findmodel->email);
}

//아이디 정보 보여주는 함수
void view_customper_showId(customerTreeNode *root)
{
	if(root)
	{
		printf("───────────────────\n");
		printf("찾으시는 아이디는 : ' %s ' 입니다.\n",root->customer.id);
		printf("───────────────────\n");
	}
	else
	{
		printf("아이디를 찾으실수 없습니다.\n");
	}
	getchar();
}