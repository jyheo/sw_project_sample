#ifndef MODEL_H
#define MODEL_H
#include "book.h"

//관리자 로그인 확인하는 함수
int model_admin_login(personModel *checkAdmin);

//현재 접속한 고객 정보 돌려주는 함수
connectionBinder model_customer_getCustomerName();

//회원가입 함수
void model_customer_joinMember(personModel *cs) ;

//책 제목으로 책 추가함수
void model_book_insertTitle(char *title) ;

//책 인덱스로 검색하는 함수
void model_book_searchIndex(TreeNode  *root, int idx);

//책 제목으로 검색하는 함수
TreeNode  *model_book_searchTitle(char *title);

//책정보 초기화하는함수
void model_book_init();

//책반납 하는 함수
void model_book_returnUpdate(TreeNode *book);

//책 빌리는 함수
void model_book_loanUpdate(TreeNode *book);

//해당 index 책정보 반환하는 함수
TreeNode* model_book_returnIndex(int idx);

//로그인 하는 함수
int model_customer_login(personModel *cs);

//책정보 삭제 함수
void model_book_delete(char *title);

// 삭제 함수
void model_customer_delete(char *id);

//비밀번호를 찾는함수
customerTreeNode  *model_customer_findPassword(personModel *model);

//고객 아이디 전달해주는 함수
customerTreeNode* model_customer_returnId(personModel *model );

//책 모든 정보 불러오기
void model_book_showAll();

//모든 고객 정보 불러오기
void model_customer_showAll();

//책 위치 변경하는 함수
void medel_dook_change(TreeNode *search, int position);

#endif