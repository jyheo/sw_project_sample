#include "book.h"
#include "model.h"
#include "view.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//책 대출
void con_book_loan(TreeNode *book)
{
	//대출성공
	if(book)
	{
		if(book->book.checkout == 0)
		{
			view_message("대출되었습니다.");
			//책 정보 업데이트
			model_book_loanUpdate(book);
			return ;
		}
		//대출실패
		else if(book->book.checkout == 1)
		{
			view_message("대출중 책입니다.");
			return ;
		}
	}
	//책없음
	view_message("───────────────────\n찾으시는 책이 없습니다.\n───────────────────\n");
}

// 빌리는 방법선택하는 함수
void con_book_loan_select(int select)
{
	TreeNode *search = NULL;
	int idx=0;
	char title[200];
	switch(select)
	{
		//index로 책빌림
	case 1:
		view_book_loanIndex(&idx);
		search = model_book_returnIndex(idx);
		con_book_loan(search);
		break;
		//제목으로로 책빌림
	case 2:
		view_book_loanTitle(title);
		search = model_book_searchTitle(title);
		con_book_loan(search);
		break;
	}
}

//해당 책 찾는 함수
void con_book_search_select(int select)
{
	TreeNode *search = NULL;
	int idx=0;
	char title[200];
	switch(select)
	{
	case 1:
		view_book_searchIndex(&idx);
		search = model_book_returnIndex(idx);
		view_book_showInfo(search);
		break;
	case 2:
		view_book_searchTitle(title);
		search = model_book_searchTitle(title);
		view_book_showInfo(search);
		break;
	}
}


//책 반납 함수
void con_book_return(TreeNode *book)
{
	//반납
	if(book)
	{
		connectionBinder current_customer = model_customer_getCustomerName();
		if(book->book.checkout && strcmp(book->book.borrower,current_customer.name) == 0)
		{
			view_message("반납되었습니다.");
			model_book_returnUpdate(book);
		}
		//대출실패
		else
		{
			view_message("반납하실필요없는 책입니다.");
		}
	}
	else
	{
		view_message("───────────────────\n반납하실 책이 없습니다.\n───────────────────\n");
	}
}

//책반납방법 선택 함수
void con_book_return_select(int select)
{
	TreeNode *search;
	int idx=0;
	char title[200];
	switch(select)
	{
	case 1:
		view_book_returnIndex(&idx);
		search = model_book_returnIndex(idx);
		con_book_return(search);
		break;
	case 2:
		view_book_returnTitle(title);
		search = model_book_searchTitle(title);
		con_book_return(search);
		break;
	}
}

//책 추가 함수
void con_book_insert()
{
	char title[200];
	view_book_insert(title);
	model_book_insertTitle(title);
}


//고객 추가하는함수
void con_customer_joinMember()
{
	personModel newCustomer;
	newCustomer = view_joinForm();
	model_customer_joinMember(&newCustomer);
}

//로그인 함수
int con_customer_login()
{
	personModel customer;
	customer = view_customer_login();

	if(model_customer_login(&customer))
	{
		view_message("로그인에 실패하였습니다.");
		return 0;
	}
	else 
	{
		view_message("접속되셨습니다");
		return 1;
	}

}

//회원 탈퇴 함수
void con_customer_delete()
{
	char id[30];
	view_customer_delete(id);
	model_customer_delete(id);
}

//관리자 로그인 함수
int con_admin_login()
{
	if(model_admin_login(&view_admin_login()))
	{
		view_message("접속되셨습니다");
		return 1;
	}
	else 
	{
		view_message("로그인에 실패하였습니다.");
		return 0;
	}
}
//책 수정함수
void con_book_change()
{
	TreeNode *search;
	char title[200];
	int position;
	view_book_change(title);
	search = model_book_searchTitle(title);
	view_book_showInfo(search);
	if(search)
	{
		position = view_book_change_position();
		medel_dook_change(search,position);
	}
}

//책 삭제함수
void con_book_delete()
{
	char title[200];
	view_book_delete(title);
	model_book_delete(title);
}

//관리자 책 관리 함수
void con_admin_book()
{
	int select;
	while(1)
	{
		select = view_admin_book_selectMenu();

		switch(select)
		{
		case 1:
			system("cls");
			model_book_showAll();
			fflush(stdin);
			getchar();
			break;
		case 2:
			con_book_insert();
			break;
		case 3:
			system("cls");
			model_book_showAll();
			con_book_change();
			break;
		case 4:
			system("cls");
			model_book_showAll();
			fflush(stdin);
			con_book_delete();
			break;
		case 5:
			return;

		}
	}

}

//관리자 고객 관리함수
void con_admin_customer()
{
	int select;
	while(1)
	{
		select = view_admin_customer_selectMenu();

		switch(select)
		{
		case 1:
			system("cls");
			model_customer_showAll();
			fflush(stdin);
			getchar();
			break;
		case 2:
			system("cls");
			model_customer_showAll();
			con_customer_delete();
			break;
		case 3:
			return;

		}
	}
}

//관리자 접속 
void con_admin()
{
	int select;
	int login = con_admin_login();
	while (login)
	{
		select = view_admin_selectMenu();
		switch(select)
		{
		case 1:
			con_admin_book();
			break;
		case 2:
			con_admin_customer();
			break;
		case 3:
			return ;
		}
	}
}

//고객 비밀번호 찾는함수
void con_customer_findPassword()
{
	personModel findmodel;
	customerTreeNode* search;
	view_customer_findPassword(&findmodel);
	search = model_customer_findPassword(&findmodel);
	view_customper_showPassword(search);

}
//로그인된 고객 관리함수
void con_customer()
{
	int select;

	do {
		select = view_login_selectMenu();
		switch(select)
		{
		case 1:
			system("cls");
			model_book_showAll();
			fflush(stdin);
			getchar();
			break;
		case 2:
			select = view_book_search_selectMenu();
			con_book_search_select(select);
			break;
		case 3:
			select = view_book_loan_selectMenu();
			con_book_loan_select( select);
			break;
		case 4:
			select = view_book_return_menuSelect();
			con_book_return_select( select);
			break;
		case 5:
			break;
		}
	}while(select != 5);
}

//아이디 찾는함수
void con_customer_searchId()
{
	personModel findmodel;
	customerTreeNode* search;
	view_customer_findId(&findmodel);
	search = model_customer_returnId(&findmodel);
	view_customper_showId(search);
}