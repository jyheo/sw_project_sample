#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "book.h"
#include "view.h"


//책 DB
TreeNode *root = NULL;
customerTreeNode *customers = NULL;
connectionBinder current_customer;
personModel admin = {"root" , "한종빈", "stompesi@gmail.com", "1234"};
TreeNode *searchBook;
customerTreeNode *searchCustomer;
//책번호로 책을 찾는함수
void model_book_searchIndex(TreeNode* search, int idx) 
{ 
	if(search){ 
		if( search->book.idx == idx )
		{
			searchBook = search;
		}
		model_book_searchIndex(search->left, idx);
		model_book_searchIndex(search->right, idx);
	} 	// 탐색에 실패했을 경우 NULL 반환
} 



//관리자 로그인 확인하는 함수
int model_admin_login(personModel *checkAdmin)
{
	if(	strcmp(admin.password, checkAdmin->password) ==0){
		return 1;
	}
	else{
		return 0;
	}
}

//현재 접속한 고객 정보 돌려주는 함수
connectionBinder model_customer_getCustomerName()
{
	return current_customer;
}

//회원가입 함수
void model_customer_joinMember(personModel *cs) 
{
	customerTreeNode *p, *t; 
	customerTreeNode *n;	 
	t = customers;
	p = NULL;
	while (t != NULL){
		if( strcmp( cs->id , t->customer.id) == 0 )
		{
			view_message("존재하는 아이디입니다.\n");
			return;
		}
		p = t;
		if( strcmp( cs->id , t->customer.id) < 0 )
		{
			t = t->left;
		}
		else
		{
			t = t->right;
		}
	}
	n = (customerTreeNode *) malloc(sizeof(customerTreeNode));
	if( n == NULL )
	{
		return;
	}
	strcpy(n->customer.id , cs->id);
	strcpy(n->customer.password , cs->password);
	strcpy(n->customer.name , cs->name);
	strcpy(n->customer.email , cs->email);
	n->left = n->right = NULL;
	if( p != NULL ) 
		if( strcmp( cs->id , p->customer.id) < 0) 
			p->left = n;
		else p->right = n;
	else customers = n;
}

//책 제목으로 책 추가함수
void model_book_insertTitle(char *title) 
{
	static int idx = 1;
	TreeNode *p, *t; 
	TreeNode *n;	 
	t = root;
	p = NULL;
	int position;
	while (t != NULL){
		if( strcmp( title , t->book.title) == 0 )
		{
			view_message("책이 이미 존재 합니다.\n");
			return;
		}
		p = t;
		if( strcmp( title , t->book.title) < 0 )
		{
			t = t->left;
		}
		else
		{
			t = t->right;
		}
	}
	n = (TreeNode *) malloc(sizeof(TreeNode));
	if( n == NULL )
	{
		return;
	}
	strcpy(n->book.title , title);
	n->book.idx = idx++;
	position = (idx % 5) + 1;

	if(position == 6 )
	{
		position = 1;
	}
	n->book.position = position ;
	n->book.checkout = 0;
	n->left = n->right = NULL;
	if( p != NULL ) 
		if( strcmp( title , p->book.title) < 0) 
			p->left = n;
		else p->right = n;
	else root = n;
}


//책 제목으로 검색하는 함수
TreeNode* model_book_searchTitle(char *title) 
{ 
	TreeNode *search = root;
	while(search != NULL){ 
		if( strcmp(title, search->book.title) ==0 )
		{
			return search;
		}
		else if( strcmp(title, search->book.title) < 0 ) 
			search = search->left; 
		else 
			search = search->right; 
	}
	return NULL;
} 

//책 위치 변경하는 함수
void medel_dook_change(TreeNode *search, int position)
{
	search->book.position = position;
	view_message("위치가 변경되었습니다.\n");
}


//책정보 초기화하는함수
void model_book_init()
{
	model_book_insertTitle("해리포터와 아즈카반의 죄수");
	model_book_insertTitle("해리포터와 불사조 기사단");
	model_book_insertTitle("어린왕자");
	model_book_insertTitle("마쉬멜로우");
	model_book_insertTitle("너는 왜 사는가?");
	model_book_insertTitle("멀리가려면 함께 가라");
	model_book_insertTitle("영어단어 3000");
	model_book_insertTitle("나는 왜 사는가?");
	model_book_insertTitle("반지의 제왕");
	model_book_insertTitle( "너는 내운명");
}

//책반납 하는 함수
void model_book_returnUpdate(TreeNode *book)
{
	book->book.checkout = 0;
}

//책 빌리는 함수
void model_book_loanUpdate(TreeNode *book)
{
	book->book.checkout = 1;
	strcpy(book->book.borrower , current_customer.name);
}


//해당 index 책정보 반환하는 함수
TreeNode* model_book_returnIndex(int idx)
{
	searchBook = NULL;
	model_book_searchIndex(root,idx);
	return searchBook;
}

//로그인 하는 함수
int model_customer_login( personModel *cs)
{ 
	customerTreeNode *search = customers;
	while(search != NULL){ 
		if ( strcmp(cs->id, search->customer.id) ==0 )
		{
			//로그인성공
			if ( strcmp(cs->password, search->customer.password) ==0 )
			{
				strcpy(current_customer.id,search->customer.id);
				strcpy(current_customer.name,search->customer.name);
				return 0;
			}
			//로그인 실패
			else
			{
				return 1;
			}
		}
		else if(  strcmp(cs->id, search->customer.id) < 0  ) 
			search = search->left; 
		else 
			search = search->right; 
	}
	return 1;
} 



//책정보 삭제 함수
void model_book_delete(char *title)
{
	TreeNode *p, *child, *succ, *succ_p, *t;
	p = NULL;
	t = root;
	while( t != NULL && strcmp(title, t->book.title) != 0 )
	{
		p = t;
		t = ( strcmp(title, t->book.title) < 0 ) ? t->left : t->right;
	}
	if( t == NULL )
	{ 
		view_message("책이 존재하지 않습니다.\n");
		return;
	}

	if( (t->left==NULL) && (t->right==NULL) )
	{ 
		if( p != NULL )
		{
			if( p->left == t )	
			{
				p->left = NULL;
			}
			else
			{
				p->right = NULL;
			}
		}
		else
		{
			root = NULL;
		}
	}

	else if((t->left==NULL)||(t->right==NULL))
	{
		child = (t->left != NULL) ? t->left : t->right;
		if( p != NULL )
		{
			if( p->left == t )
			{ 
				p->left = child;
			}
			else
			{
				p->right = child;
			}
		}
		else 
		{	
			root = child;
		}
	}
	else
	{		
		succ_p = t;
		succ = t->right;
		while(succ->left != NULL)
		{
			succ_p = succ;
			succ = succ->left;
		}
		if( succ_p->left == succ )
		{
			succ_p->left = succ->right;
		}
		else 
		{
			succ_p->right = succ->right;
		}
		t->book = succ->book;
		t = succ;
	}
	view_message("책을 삭제하였습니다.\n");
	free(t);
}


// 삭제 함수
void model_customer_delete( char *id)
{
	customerTreeNode *p, *child, *succ, *succ_p, *t;
	p = NULL;
	t = customers;
	while( t != NULL && strcmp(id, t->customer.id) != 0 )
	{
		p = t;
		t = ( strcmp(id, t->customer.id) < 0 ) ? t->left : t->right;
	}
	if( t == NULL ) 
	{
		view_message("삭제할 아이디가 존재하지 않습니다.\n");
		return;
	}
	if( (t->left==NULL) && (t->right==NULL) )
	{ 
		if( p != NULL )
		{
			if( p->left == t )	 
			{
				p->left = NULL;
			}
			else
			{
				p->right = NULL;
			}
		}
		else
		{
			customers = NULL;
		}
	}

	else if((t->left==NULL)||(t->right==NULL))
	{
		child = (t->left != NULL) ? t->left : t->right;
		if( p != NULL )
		{
			if( p->left == t )
			{
				p->left = child;
			}
			else
			{
				p->right = child;
			}
		}
		else
		{
			customers = child;
		}
	}
	else
	{		
		succ_p = t;
		succ = t->right;
		while(succ->left != NULL)
		{
			succ_p = succ;
			succ = succ->left;
		}
		if( succ_p->left == succ )
		{
			succ_p->left = succ->right;
		}
		else 
		{
			succ_p->right = succ->right;
		}
		t->customer = succ->customer;
		t = succ;
	}
	free(t);
}

//비밀번호를 찾는함수
customerTreeNode  *model_customer_findPassword(personModel *model)
{ 
	customerTreeNode *search = customers;
	while(search != NULL){ 
		if ( strcmp(model->id, search->customer.id) ==0 )
		{
			if( strcmp(model->email, search->customer.email) ==0 )
			{
				return search;
			}
			else
			{
				return NULL;
			}
		}
		else if(  strcmp(model->id, search->customer.id) < 0  ) 
			search = search->left; 
		else 
			search = search->right; 
	}
	return NULL;
} 



//고객 아이디를 찾는함수
void model_customer_searchId( customerTreeNode *root, personModel *model)
{

	if ( root){
		model_customer_searchId( root->left, model);	// 왼쪽서브트리 순회
		model_customer_searchId( root->right, model);	// 오른쪽서브트리순회
		if(!(strcmp(root->customer.name, model->name)) && !(strcmp(root->customer.email, model->email)))
		{
			searchCustomer = root;
		}
	}
}

//고객 아이디 전달해주는 함수
customerTreeNode* model_customer_returnId(personModel *model )
{
	searchCustomer = NULL;
	model_customer_searchId(customers,model);
	return searchCustomer;
}
//책하나 정보 호출
void book_showAll(TreeNode *root)
{
	if(root)
	{
		view_book_showInfo(root);
		book_showAll(root->left);
		book_showAll(root->right);
	}
}

//책 모든 정보 불러오기
void model_book_showAll()
{
	book_showAll(root);
}
//고객 한명 정보 호출
void customer_showAll(customerTreeNode *root)
{
	if(root)
	{
		view_customer_showAll(root);
		customer_showAll(root->left);
		customer_showAll(root->right);

	}
}

//모든 고객 정보 불러오기
void model_customer_showAll()
{
	customer_showAll(customers);
}
