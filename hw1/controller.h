#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "book.h"

//책 대출
void con_book_loan(TreeNode *book);

// 빌리는 방법선택하는 함수
void con_book_loan_select(int select);

//해당 책 찾는 함수
void con_book_search_select(int select);

//책 반납 함수
void con_book_return(TreeNode *book);

//책반납방법 선택 함수
void con_book_return_select(int select);

//책 추가 함수
void con_book_insert();

//고객 추가하는함수
void con_customer_joinMember();

//로그인 함수
int con_customer_login();

//책 삭제함수
void con_book_delete();

//회원 탈퇴 함수
void con_customer_delete();

//관리자 로그인 함수
int con_admin_login();

//관리자 책 관리 함수
void con_admin_book();

//관리자 고객 관리함수
void con_admin_customer();

//관리자 접속 
void con_admin();

//고객 비밀번호 찾는함수
void con_customer_findPassword();

//로그인된 고객 관리함수
void con_customer();

//아이디 찾는함수
void con_customer_searchId();

//책수정함수
void con_book_change();

#endif