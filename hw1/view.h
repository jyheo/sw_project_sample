
#ifndef VIEW_H
#define VIEW_H

//프로그램 시작시 보여주는 함수
int view_();

//고객 로그인시 초기화면
int view_login_selectMenu();

//회원가입 방법보여주는 함수 
personModel view_joinForm();

//관리자 로그인시 초기 화면
int view_admin_selectMenu();

//도서검색 방법 선택 화면
int view_book_search_selectMenu();

//도서 정보 보여주는 함수
void view_book_showInfo(TreeNode *root);

//도서 인덱스로 검사하는 함수
void view_book_searchIndex(int *idx);

//도서 제목으로 검사하는 함수
void view_book_searchTitle(char *title);

//빌리는 방법 선택하는 화면
int view_book_loan_selectMenu();

//인덱스로 책 빌리는 함수
void view_book_loanIndex(int *idx);

//제목으로 책빌리는 함수
void view_book_loanTitle(char *title);

//책 반납 방법 선택함수
int view_book_return_menuSelect();

//반납할 책번호를 입력받는 함수
void view_book_returnIndex(int *idx);

//반납할 책제목을 입력받는 함수
void view_book_returnTitle(char *title);

//추가할 책이름을 받는 함수
void view_book_insert(char *title);

//삭제할 책 이름을 받는함수
void view_book_delete(char *title);

//관리자 로그인 비밀번호 받는함수
personModel view_admin_login();

//로그인 할 고객 정보를 받는함수
personModel view_customer_login();

//관리자 책관리 매뉴선택함수
int view_admin_book_selectMenu();

//관리자 고객 매뉴선택함수
int view_admin_customer_selectMenu();

//모든 고객 정보를 보여주는함수
void view_customer_showAll(customerTreeNode *root);

//탈퇴시킬 고객 정보를 받는함수
void view_customer_delete(char *title);

//비밀번호 찾을때 관련 정보를 받는함수
void view_customer_findPassword(personModel *findmodel);

//도서 정보 보여주는 함수
void view_customper_showPassword(customerTreeNode *root);

//메시지 보여주는 함수
void view_message(char *msg);

//아이디 찾을때 관련 정보를 받는함수
void view_customer_findId(personModel *findmodel);

//아이디 정보 보여주는 함수
void view_customper_showId(customerTreeNode *root);

//수정할 책 이름을 받는함수
void view_book_change(char *title);

//책수정할 위치 받는함수
int view_book_change_position();
#endif