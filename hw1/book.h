#ifndef BOOK_H
#define BOOK_H

typedef struct bookModel
{
	int idx;
	char title[200];
	char borrower[30];
	int position;
	int checkout;
}bookModel;

typedef struct TreeNode
{
	bookModel book;
	TreeNode *left, *right;
}TreeNode;

typedef struct personModel
{
	char id[20];
	char name[30];
	char email[50];
	char password[20];
}personModel;

typedef struct customerTreeNode
{
	personModel customer;
	customerTreeNode *left, *right;
}customerTreeNode;

typedef struct connectionBinder
{
	char id[20];
	char name[30];
}connectionBinder;

#endif