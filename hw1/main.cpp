//관리자기능및 비밀번호 찾기등등 해야함

#include "book.h"
#include "controller.h"
#include "view.h"
#include "model.h"
#include <stdlib.h>
#include <stdio.h>

int main()
{
	int select;
	model_book_init();

	while(1)
	{
		select = view_();

		switch(select)
		{
		case 1:
			//로그인 화면 보여주기
			if (con_customer_login())
			{
				con_customer();
			}
			break;
		case 2:
			//회원가입
			con_customer_joinMember();
			break;
		case 3:
			//아이디 찾기.
			con_customer_searchId();
			break;
		case 4:
			//비밀번호 찾기 보여주기
			con_customer_findPassword();
			break;
		case 5:
			//관리자 모드
			con_admin();	
			break;
		case 6:
			return 0;
		}

	}

	return 0;
}