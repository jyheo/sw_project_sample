#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STRING_MAX 1024
int main(int argc, char *argv[])
{
	char inputData[STRING_MAX]={0,};
	char query[STRING_MAX] = {0,};
	double result;
	FILE *pResult, *read, *write;

	if(argc <= 1 || argc >=3)
	{
		printf("잘못된 명령어입니다.\n");
	}
	else
	{

		printf("★ 불계산기 테스트기 입니다. ★\n\n");
		read = fopen(argv[1],"r");
		write = fopen("result.txt","w");
		if(read==NULL)
		{
			printf("파일을 열수 없습니다.");
			return 0;
		}
		while(!feof(read))
		{
			fscanf(read,"%s",inputData);
			printf("%s\t",inputData);
			fprintf(write,"%s\t",inputData);
			strtok(inputData, "=");
			strcpy(query,"calculator.exe \"");
			strcat(query,inputData);
			strcat(query,"\"");

			pResult = _popen(query, "r");

			if(pResult==NULL)
			{
				printf("불계산기를 실행할수 없습니다. (테스트 파일 열기 실패)");
				return 0;
			}

			fgets(stdin, STRING_MAX, pResult);
			fscanf(pResult, "%lf", &result);

			if(atof(strtok(NULL, "")) == result)
			{
				printf("정답입니다\n");
				fprintf(write,"정답입니다\n");
			}
			else
			{
				if(result - (int)result > 0)
				{
					printf("틀렸습니다.\t 답 : %.2lf\n",result);
					fprintf(write,"틀렸습니다.\t 답 : %.2lf\n",result);
				}
				else
				{
					printf("틀렸습니다.\t 답 : %d\n",(int)result);
					fprintf(write,"틀렸습니다.\t 답 : %d\n",(int)result);
				}
			}
			_pclose(pResult);
		}

		printf("\n★ 결과는 result.txt파일로 저장하였습니다. ★\n");
		fclose(write);
		fclose(read);
	}
	return 0;
}
