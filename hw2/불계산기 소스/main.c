/*
    Command Line Calculator
    Copyright (C) 2010 tichy_ijon (tichy_ijon@users.sourceforge.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include "dstr.h"
#include "calc.h"

int main(int argc, char *argv[])
{
	Node *fifo = NULL, *stack = NULL;
	double result;
	if (argc > 1)
	{
		// conversion to RPN
		// RPN (Reverse Polish Notation == Postfix notation)
		int result;
		if ((result = RPN_convert(argv[1], &fifo)))
			return result;
		print_fifo(fifo);
		// RPN calculation
		if ((result = RPN_calculate(fifo, &stack)))
			return result;
		print_stack(stack);
		return 0;
	}
	printf("COMMAND LINE CALCULATOR v0.3.1\n");
	printf("Copyright (C) 2010 tichy_ijon (tichy_ijon@users.sourceforge.net).\n");
	printf("Command Line Calculator comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.txt.\n");
	return 0;
}



