/*
    Command Line Calculator
    Copyright (C) 2010 tichy_ijon (tichy_ijon@users.sourceforge.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef _DSTR_H_
#define _DSTR_H_

// FUNCTIONS

#define F_UDEF  0
#define F_SIN   1
#define F_COS   2
#define F_TAN   3
#define F_LOG   4
#define F_SQRT  5

// NODE TYPES

#define T_VALUE 0      // node is value
#define T_OP1   1      // node is 1 argument operator
#define T_OP2   2      // node is 2 arguments operator
#define T_FUN   3      // node is function
#define T_VAR   4      // node is variable
#define T_UNDEF 9      // node type is not defined yet

// NODE STRUCT

typedef struct node {  // elementary data for stack and fifo
	unsigned char type; // node type (operator, value etc...)
	union {
		double val;      // value
		char op;         // operator (1 or 2 argument)
		int fun;         // function (defined by id number)
		int var;         // variable (defined by id number)
	} data;
	struct node *next;
} Node;

// STACK OPERATIONS

// push ndata to stack pointed by *top (*top is modified)
void push(Node **top, Node ndata);

// pop *ndata from stack pointed by *top (*top is modified)
void pop(Node **top, Node *ndata);

// print stack values
void print_stack(Node *top);

// FIFO OPERATIONS

// add ndata at the end of fifo pointed by *act (that is modified after), start of fifo is pointed by *first
void add(Node **first, Node **act, Node ndata);

// get *ndata from begin of fifo pointed by *first (that is modified after)
void get(Node **first, Node *ndata);

// print fifo values
void print_fifo(Node *first);

#endif



