/*
    Command Line Calculator
    Copyright (C) 2010 tichy_ijon (tichy_ijon@users.sourceforge.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef _CALC_H_
#define _CALC_H_

#include "dstr.h"

#define CALC_OK               0
#define CALC_SYNTAX_ERROR     1
#define CALC_UNKNOWN_OPERATOR 2

// convert expression expr to RPN as a fifo pointed by *fifo_first, return error code or 0 if OK
int RPN_convert(char *expr, Node **fifo_first);

// calculate RPN (pointed by fifo) using stack pointed by *stack, return error code or 0 if OK
int RPN_calculate(Node *fifo, Node **stack);

#endif



