/*
    Command Line Calculator
    Copyright (C) 2010 tichy_ijon (tichy_ijon@users.sourceforge.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include "calc.h"

int get_pri(Node n)
{
	if (n.type == T_OP1) {
		switch (n.data.op) {
			case '+': case '-': return 1;
			case '!':			return 2;
			default :           return 0;
		}
	} else if (n.type == T_OP2) {
		switch (n.data.op) {
			case '^':           return 2;
			case '*': case '/': return 3;
			case '+': case '-': return 4;
			case '&': case '|': case '#': return 5;

			default:            return 0;
		}
	}

	return 0;
}

double calc_1arg(char op, double v)
{
	switch (op) {
		case '+': return v;
		case '-': return -v;
		case '!': return !v;
		default:  return 0;
	}
}

double calc_2arg(char op, double v1, double v2)
{
	switch (op) {
		case '+': return v1+v2;
		case '-': return v1-v2;
		case '*': return v1*v2;
		case '/': return v1/v2;
		case '^': return pow(v1, v2);
		case '&': return (double)((int)v1&(int)v2);
		case '|': return (double)((int)v1|(int)v2);
		case '#': return (double)((int)v1^(int)v2);
		default:  return 0;
	}
}

double calc_fun(int fun, double v)
{
	switch (fun) {
		case F_SIN:  return sin(v*M_PI/180);
		case F_COS:  return cos(v*M_PI/180);
		case F_TAN:  return tan(v*M_PI/180);
		case F_LOG:  return log10(v);
		case F_SQRT: return sqrt(v);
		default: return 0;
	}
}

int strtofun(char *s, char **endp)
{
	if (!strncmp(s, "sin(", 4)) {
		*endp = s + 3;
		return F_SIN;
	} else if (!strncmp(s, "cos(", 4)) {
		*endp = s + 3;
		return F_COS;
	} else if (!strncmp(s, "tan(", 4)) {
		*endp = s + 3;
		return F_TAN;
	} else if (!strncmp(s, "log(", 4)) {
		*endp = s + 3;
		return F_LOG;
	} else if (!strncmp(s, "sqrt(", 5)) {
		*endp = s + 4;
		return F_SQRT;			
	} else
		return F_UDEF;
}

int RPN_convert(char *expr, Node **fifo_first)
{
	Node n1 = {0,}, n2, *fifo = NULL, *stack = NULL;
	n1.type = T_UNDEF;
	do {
		if (isdigit(*expr)) {
			n1.type = T_VALUE;
			n1.data.val = strtod(expr, &expr);
			expr--;
			add(fifo_first, &fifo, n1);
		} else if (isalpha(*expr)) {
			n1.type = T_FUN;
			if (!(n1.data.fun = strtofun(expr, &expr))) {
				printf("Syntax error in '%s'!\n", expr);
				return CALC_SYNTAX_ERROR;			
			}
			expr--;
			push(&stack, n1);
		} else {
			n1.type = (!n1.type || n1.data.op == ')') ? T_OP2 : T_OP1;
			n1.data.op = *expr;
			if (!get_pri(n1) && n1.data.op != '(' && n1.data.op != ')') {
				printf("Unknown or improperly used operator '%c'!\n", n1.data.op);
				return CALC_UNKNOWN_OPERATOR;
			}
			if (n1.data.op == ')') {
				pop(&stack, &n2);
				while (stack && n2.data.op != '(') {
					add(fifo_first, &fifo, n2);
					pop(&stack, &n2);
				}
				if (stack) {
					pop(&stack, &n2);
					if (n2.type == T_FUN)
						add(fifo_first, &fifo, n2);
					else
						push(&stack, n2);
				}
			}
			else {
				if (n1.data.op != '(')
					while (stack && stack->data.op != '(' && get_pri(*stack) <= get_pri(n1)) {
						pop(&stack, &n2);
						add(fifo_first, &fifo, n2);
					}
				push(&stack, n1);
			}			
		}	

	} while (*++expr != '\0');
	while (stack) {
		pop(&stack, &n2);
		add(fifo_first, &fifo, n2);
	}
	return CALC_OK;
}


int RPN_calculate(Node *fifo, Node **stack)
{
	Node n1, n2;
	double v1, v2;
	while (fifo) {
		get(&fifo, &n1);
		switch (n1.type) {
			case T_VALUE:
				push(stack, n1);
				break;
			case T_OP1: case T_OP2:
				pop(stack, &n2);
				v1 = n2.data.val;
				if (n1.type == T_OP1) {
					n2.type = T_VALUE;
					n2.data.val = calc_1arg(n1.data.op, v1);
				} else {
					pop(stack, &n2);
					v2 = n2.data.val;
					n2.type = T_VALUE;
					n2.data.val = calc_2arg(n1.data.op, v2, v1);
				}
				push(stack, n2);
				break;
			case T_FUN:
				pop(stack, &n2);
				v1 = n2.data.val;
				n2.type = T_VALUE;
				n2.data.val = calc_fun(n1.data.fun, v1);
				push(stack, n2);				
				break;
		}			
	}
	return CALC_OK;
}



