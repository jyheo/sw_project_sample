/*
    Command Line Calculator
    Copyright (C) 2010 tichy_ijon (tichy_ijon@users.sourceforge.net)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "dstr.h"

void print_node(Node n)
{
	switch (n.type) {
		case T_VALUE: 
			printf("%.*g ", DBL_DIG, n.data.val);
			break;
		case T_OP1: case T_OP2:
			printf("%c ", n.data.op);
			break;
		case T_FUN:
			printf("F%02d ", n.data.fun);
			break;
		case T_VAR:
			// not implemented yet
			break;
		case T_UNDEF: default:
			// ??
			break;
	}
}

void push(Node **top, Node ndata)
{
	Node *np = (Node *) malloc(sizeof(Node));
	*np = ndata;
	np->next = *top;
	*top = np;
}

void pop(Node **top, Node *ndata)
{
	Node *np = *top;
	if (np) {
		*ndata = *np;
		*top = np->next;
		free((void *) np);
	} else
		ndata = NULL;
}

void print_stack(Node *top)
{
	if (top) {
		//top->type ? printf("%c ", top->data.op) : printf("%.*g ", DBL_DIG, top->data.val);
		print_node(*top);
		print_stack(top->next);
	}
	else
		printf("\n");
}

void add(Node **first, Node **act, Node ndata)
{
	Node *np = (Node *) malloc(sizeof(Node));
	*np = ndata;
	np->next = NULL;
	if (*act)
		(*act)->next = np;
	*act = np;
	if (!*first)
		*first = *act;
}

void get(Node **first, Node *ndata)
{
	pop(first, ndata);
}

void print_fifo(Node *first)
{
	if (first) {
		//first->type ? printf("%c ", first->data.op) : printf("%.*g ", DBL_DIG, first->data.val);
		print_node(*first);
		print_fifo(first->next);
	}
	else
		printf("\n");
}



